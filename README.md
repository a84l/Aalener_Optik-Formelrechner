# Aalener Optik Formelrechner

The Aalener Optik-Formelrechner enables a quick and easy option to calculate a bunch of optical functions.

<a href="https://www.gnu.org/licenses/gpl-3.0"><img src="https://www.gnu.org/graphics/gplv3-127x51.png" alt="GNU GPL v3.0"/></a>
<a href="https://play.google.com/store/apps/details?id=de.HS_Aalen.don"><img src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png" alt="Get it on Google Play" height="90"/></a>
<a href="https://f-droid.org/packages/de.HS_Aalen.don/"><img src="https://f-droid.org/badge/get-it-on.png" alt="Get it on F-Droid" height="90"/></a>

# Description

At the moment the following features are implemented:

- paraxial raytracing
- exact raytracing for an object on the opitcal axis
- Calculation of oblique crossed cylinders with optional sphere
- Calculation of refraction index
- Calculation of relative transmission
- Calculation of radii for lenses which are free of spherical aberration or coma
- Calculation and visualization for error of refraction, astigmatism, 
  distortion, volume, weight and dimensions of spherical toric lenses
- Conversion of Back Vertex Distances (eg. glasses to contact lenses)

This application was part of a bachelor thesis for the study program opthalmic optics and audiology at the Aalen University. <br>
It is meant to check calculations and takes part in the interactive lectures. <br>
The app is also interesting for opticians and students of other scientific courses.

# Disclaimer

Please be advised that the whole application is in german only. <br>
If you are an optician you will be able to understand most of the functions by their formula symbols.

# Beschreibung

Der Aalener Optik-Formelrechner ermöglicht die schnelle und einfache Art eine Vielzahl von optischen Funktionen zu berechnen.

Aktuell sind folgende Funktionen implementiert:
- Paraxiales Raytracing
- Exaktes Raytracing für einen beliebigen von einem Achsenpunkt 
  ausgehenden Lichtstrahl
- Berechnung von schiefgekreuzten Zylindern mit optionalem sphärischen Anteil
- Berechnung von Brechungsindices nach der Schottgleichung
- Berechnung der relativen Transmission
- Ermittlung der Radien für komafreie Gläser und minimale sphärische Aberration
- Berechnung und grafische Darstellung von Refraktionsfehler, Astigmatismus 
  und Verzeichnung sowie Volumen, Gewicht und Abmessungen von individuellen 
  sphäro-torischen Brillengläsern
- HSA-Umrechner

Die App wurde im Rahmen einer Bachelorarbeit für den Studiengang Augenoptik und Hörakustik der Hochschule Aalen entwickelt. <br>
Dort dient sie den Studierenden als Berechnungskontrolle und wird darüber hinaus als Plattform für interaktive Vorlesungen verwendet.

# Privacy policy

The app is free of ads, does not need an internet connection or permission with access to the device. <br>
<a href="https://www.hs-aalen.de/pages/datenschutz-app">Pricacy policy in german</a>

Die App ist werbefrei, benötigt keine Internetverbindung und erfordert keine Berechtigungen die auf das Gerät zugreifen. <br>
<a href="https://www.hs-aalen.de/pages/datenschutz-app">Datenschutzerklärung auf deutsch</a>

# Releases

Version - <a href="https://gitlab.com/HS_Aalen_RN/Aalener_Optik-Formelrechner/tags/v1.10">1.10</a> - 
<a href="https://gitlab.com/HS_Aalen_RN/Aalener_Optik-Formelrechner/blob/master/Releases/AOF_1.10.apk">Download</a>

<a href="https://gitlab.com/HS_Aalen_RN/Aalener_Optik-Formelrechner/blob/master/CHANGELOG">Changelog</a>

# Licence

<b>Aalener Optik-Formelrechner</b> <br>
Copyright (C) 2017-2020 René Nierath <br>

This program is free software: <br>
you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. <br>
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. <br>
If not, see <https://www.gnu.org/licenses/>.
