package de.HS_Aalen.OptikFormelrechner;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.Spinner;
import android.widget.TextView;



public class Shape extends AppCompatActivity {

    Spinner shape_s, shape_D;
    CheckBox cb_s_munendlich, cb_s_unendlich;
    EditText et_DGes, et_s, et_n;
    TextView txt_shape_s_mm, txt_shape_D_mm, P_result, S_sph_result, S_koma_result,
            r1_sph_result, r2_sph_result, r1_koma_result, r2_koma_result;
    ArrayAdapter<CharSequence> adapter;
    GridLayout grid_result;
    String s_auswahl, D_auswahl;
    double P, f_1, s, S_sph, S_koma, r1_sph, r2_sph, r1_koma, r2_koma, n, Umrechner;


    final Context context = this;
    Button bt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shape);

        // Verhindert das Öffnen des Tastaturlayouts beim Starten der Activity
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        grid_result = (GridLayout) findViewById(R.id.grid_result);

        cb_s_munendlich = (CheckBox) findViewById(R.id.cb_shape_s_munendlich);
        cb_s_unendlich = (CheckBox) findViewById(R.id.cb_shape_s_unendlich);
        et_DGes = (EditText) findViewById(R.id.et_DGes);
        et_DGes.requestFocus();
        et_n = (EditText) findViewById(R.id.et_n);
        et_s = (EditText) findViewById(R.id.et_s);
        txt_shape_s_mm = (TextView) findViewById(R.id.txt_shape_s_mm);
        txt_shape_D_mm = (TextView) findViewById(R.id.txt_D_dpt);

        shape_D = (Spinner) findViewById(R.id.spinner_shape_D);
        adapter = ArrayAdapter.createFromResource(this, R.array.spinner_shape_D,
                android.R.layout.simple_list_item_1);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        shape_D.setAdapter(adapter);
        shape_D.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                if (position == 0) {
                    txt_shape_D_mm.setText(" dpt");
                    D_auswahl = "D<sub><small>Ges<sub><small><small>Luft</small>" +
                            "</small></sub></small></sub>";
                    String Alarm = et_DGes.getText().toString();
                    if (Alarm.equals("") || Alarm.equals("0")) {
                    } else {
                        Umrechner = Double.parseDouble(et_DGes.getText().toString());
                        Umrechner = 1 / (Umrechner / 1000);
                        et_DGes.setText(Double.toString(Math.round(Umrechner * 10000) / 10000.0));
                    }
                } else {
                    txt_shape_D_mm.setText(" mm");
                    D_auswahl = "f'";
                    String Alarm = et_DGes.getText().toString();
                    if (Alarm.equals("") || Alarm.equals("0")) {
                    } else {
                        Umrechner = Double.parseDouble(et_DGes.getText().toString());
                        Umrechner = (1 / Umrechner) * 1000;
                        et_DGes.setText(Double.toString(Math.round(Umrechner * 10000) / 10000.0));
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        shape_s = (Spinner) findViewById(R.id.shape_s);
        adapter = ArrayAdapter.createFromResource(this, R.array.shape_s,
                android.R.layout.simple_list_item_1);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        shape_s.setAdapter(adapter);
        shape_s.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                if (position == 0) {
                    cb_s_munendlich.setChecked(false);
                    cb_s_unendlich.setChecked(false);
                    cb_s_munendlich.setVisibility(View.VISIBLE);
                    cb_s_unendlich.setVisibility(View.GONE);
                    s_auswahl = "s";
                } else {
                    cb_s_munendlich.setChecked(false);
                    cb_s_unendlich.setChecked(false);
                    cb_s_munendlich.setVisibility(View.GONE);
                    cb_s_unendlich.setVisibility(View.VISIBLE);
                    s_auswahl = "s'";
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        cb_s_munendlich.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (cb_s_munendlich.isChecked()) {
                    et_s.setText("");
                    et_s.setVisibility(View.GONE);
                    txt_shape_s_mm.setVisibility(View.GONE);
                } else {
                    et_s.setVisibility(View.VISIBLE);
                    txt_shape_s_mm.setVisibility(View.VISIBLE);
                }
            }
        });

        cb_s_unendlich.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (cb_s_unendlich.isChecked()) {
                    et_s.setText("");
                    et_s.setVisibility(View.GONE);
                    txt_shape_s_mm.setVisibility(View.GONE);
                } else {
                    et_s.setVisibility(View.VISIBLE);
                    txt_shape_s_mm.setVisibility(View.VISIBLE);
                }
            }
        });


        bt = (Button) findViewById(R.id.bT);
        bt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                String Alarm = et_DGes.getText().toString();
                if(Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") ||
                        Alarm.equals("+")) {
                    AlertDialog.Builder AlarmBox = new AlertDialog.Builder(context);
                    AlarmBox.setTitle("Leeres Feld!");
                    AlarmBox.setMessage(Html.fromHtml(D_auswahl + " ist leer<br>"));
                    AlarmBox.setNeutralButton("OK", null);

                    AlertDialog dialog = AlarmBox.create();
                    dialog.show();
                    return;
                }

                Alarm = et_s.getText().toString();
                if (cb_s_munendlich.isChecked() || cb_s_unendlich.isChecked()) {
                } else {

                    if(Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") ||
                            Alarm.equals("+")) {
                    AlertDialog.Builder AlarmBox = new AlertDialog.Builder(context);
                    AlarmBox.setTitle("Leeres Feld!");
                    AlarmBox.setMessage(s_auswahl + " ist leer");
                    AlarmBox.setNeutralButton("OK", null);

                    AlertDialog dialog = AlarmBox.create();
                    dialog.show();
                    return;
                    }

                }


                Alarm = et_n.getText().toString();
                if(Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") ||
                        Alarm.equals("+")) {
                    AlertDialog.Builder AlarmBox = new AlertDialog.Builder(context);
                    AlarmBox.setTitle("Leeres Feld!");
                    AlarmBox.setMessage("n ist leer");
                    AlarmBox.setNeutralButton("OK", null);

                    AlertDialog dialog = AlarmBox.create();
                    dialog.show();
                    return;
                }

                grid_result.setVisibility(View.VISIBLE);

                if (cb_s_munendlich.isChecked()) {
                    if (D_auswahl.equals("f'")) {
                        f_1 = Double.parseDouble(et_DGes.getText().toString());
                    } else {
                        f_1 = Double.parseDouble(et_DGes.getText().toString());
                        f_1 = (1 / f_1) * 1000;
                    }
                    P = -1;
                } else if (cb_s_unendlich.isChecked()) {
                    if (D_auswahl.equals("f'")) {
                        f_1 = Double.parseDouble(et_DGes.getText().toString());
                    } else {
                        f_1 = Double.parseDouble(et_DGes.getText().toString());
                        f_1 = (1 / f_1) * 1000;
                    }
                    P = 1;
                } else {
                    if (s_auswahl.equals("s")) {
                        if (D_auswahl.equals("f'")) {
                            f_1 = Double.parseDouble(et_DGes.getText().toString());
                            s = Double.parseDouble(et_s.getText().toString());
                            P = -((( 2 * f_1) / s) + 1);
                        } else {
                            f_1 = Double.parseDouble(et_DGes.getText().toString());
                            f_1 = (1 / f_1) * 1000;
                            s = Double.parseDouble(et_s.getText().toString());
                            P = -(((2 * f_1) / s) + 1);
                        }
                    } else {
                        if (D_auswahl.equals("f'")) {
                            f_1 = Double.parseDouble(et_DGes.getText().toString());
                            s = Double.parseDouble(et_s.getText().toString());
                            P = 1 - ((2 * f_1) / s);
                        } else {
                            f_1 = Double.parseDouble(et_DGes.getText().toString());
                            f_1 = (1 / f_1) * 1000;
                            s = Double.parseDouble(et_s.getText().toString());
                            P = 1 - ((2 * f_1) / s);
                        }
                    }
                }

                n = Double.parseDouble(et_n.getText().toString());

                S_sph = -((2 * (Math.pow(n, 2) - 1)) / (n + 2)) * P;
                r1_sph = (2 * f_1 *(n - 1)) / (S_sph + 1);
                r2_sph = (2 * f_1 *(n - 1)) / (S_sph - 1);

                S_koma = -(((2 * Math.pow(n, 2)) - n - 1) / (n + 1)) * P;
                r1_koma = (2 * f_1 *(n - 1)) / (S_koma + 1);
                r2_koma = (2 * f_1 *(n - 1)) / (S_koma - 1);

                P_result = (TextView) findViewById(R.id.txt_P_result);
                S_sph_result = (TextView) findViewById(R.id.txt_shape_Shape_sph_result);
                S_koma_result = (TextView) findViewById(R.id.txt_shape_Shape_koma_result);

                r1_sph_result = (TextView) findViewById(R.id.txt_shape_r1_sph_result);
                r2_sph_result = (TextView) findViewById(R.id.txt_shape_r2_sph_result);

                r1_koma_result = (TextView) findViewById(R.id.txt_shape_r1_koma_result);
                r2_koma_result = (TextView) findViewById(R.id.txt_shape_r2_koma_result);

                P_result.setText(Double.toString(Math.round(P * 10000) / 10000.0));
                String Ausgabe = P_result.getText().toString();
                if (Ausgabe.length() > 12) {
                   P_result.setText(String.format("%9.4e", P).replace(",","."));
                }

                S_sph_result.setText(Double.toString(Math.round(S_sph * 10000) / 10000.0));
                Ausgabe = S_sph_result.getText().toString();
                if (Ausgabe.length() > 12) {
                  S_sph_result.setText(String.format("%9.4e", S_sph).replace(",","."));
                }

                S_koma_result.setText(Double.toString(Math.round(S_koma * 10000) / 10000.0));
                Ausgabe = S_koma_result.getText().toString();
                if (Ausgabe.length() > 12) {
                    S_koma_result.setText(String.format("%9.4e", S_koma).replace(",","."));
                }

                r1_sph_result.setText(Double.toString(Math.round(r1_sph * 10000) / 10000.0));
                Ausgabe = r1_sph_result.getText().toString();
                if (Ausgabe.length() > 12) {
                    r1_sph_result.setText(String.format("%9.4e", r1_sph).replace(",","."));
                }

                r2_sph_result.setText(Double.toString(Math.round(r2_sph * 10000) / 10000.0));
                Ausgabe = r2_sph_result.getText().toString();
                if (Ausgabe.length() > 12) {
                    r2_sph_result.setText(String.format("%9.4e", r2_sph).replace(",","."));
                }

                r1_koma_result.setText(Double.toString(Math.round(r1_koma * 10000) / 10000.0));
                Ausgabe = r1_koma_result.getText().toString();
                if (Ausgabe.length() > 12) {
                    r1_koma_result.setText(String.format("%9.4e", r1_koma).replace(",","."));
                }

                r2_koma_result.setText(Double.toString(Math.round(r2_koma * 10000) / 10000.0));
                Ausgabe = r2_koma_result.getText().toString();
                if (Ausgabe.length() > 12) {
                    r2_koma_result.setText(String.format("%9.4e", r2_koma).replace(",","."));
                }

                }
        });
    }
}