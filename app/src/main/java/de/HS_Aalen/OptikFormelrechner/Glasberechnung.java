package de.HS_Aalen.OptikFormelrechner;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;


public class Glasberechnung extends AppCompatActivity {


    final Context context = this;


    // Beschreibung Visibility gone
    private TextView txt_HS2_wunsch, txt_glasrechner_hs2_out_einheit, txt_F2_wkn,
            txt_glasrechner_f2_wkn_out_einheit, txt_F3_wkn, txt_glasrechner_f3_wkn_out_einheit,
            txt_F3_real, txt_glasrechner_f3_real_out_einheit, txt_R3,
            txt_glasrechner_r3_out_einheit, txt_DR3, txt_glasrechner_dr3_out_einheit,
            txt_glasrechner_hs2_refra, txt_glasrechner_hs2_asti, txt_glasrechner_hs2_vz;

    // Ausgabe Visibility gone
    private TextView txt_glasrechner_hs2_out, txt_glasrechner_f2_wkn_out,
            txt_glasrechner_f3_wkn_out, txt_glasrechner_f3_real_out, txt_glasrechner_r3_out,
            txt_glasrechner_dr3_out, txt_glasrechner_refra_hs2_out,
            txt_glasrechner_asti_hs2_out, txt_glasrechner_vz_hs2_out;

    // Ausgabe Visibility true
    private TextView txt_glasrechner_hs1_out, txt_glasrechner_f1_real_out,
            txt_glasrechner_f2_real_out, txt_glasrechner_r1_out, txt_glasrechner_r2_out,
            txt_glasrechner_dr2_out, txt_glasrechner_bauhoehe_out, txt_glasrechner_volumen_out,
            txt_glasrechner_gewicht_out, txt_glasrechner_grad_refra_out,
            txt_glasrechner_refra_hs1_out, txt_glasrechner_grad_asti_out,
            txt_glasrechner_asti_hs1_out, txt_glasrechner_grad_vz_out, txt_glasrechner_vz_hs1_out,
            txt_glasrechner_d_einheit, txt_glasrechner_d, txt_glasrechner_dr_einheit,
            txt_glasrechner_dr, txt_F1_real, txt_F2_real;

    private EditText et_glasrechner_sph_in, et_glasrechner_cyl_in, et_glasrechner_f1_in,
            et_glasrechner_n_in, et_glasrechner_d_in, et_glasrechner_dr_in,
            et_glasrechner_durchmesser_in, et_glasrechner_rho_in, et_glasrechner_b_1_in,
            et_glasrechner_s_1_in, et_glasrechner_blickwinkel_in, et_glasrechner_abstufung_in;
    private CheckBox cb_glasrechner_werkzeugindex, cb_glasrechner_flaechen_gerundet;
    private GridLayout grid_glasrechner_result;
    private Button bt;

    private LineChart lc_glasrechner_refra_out, lc_glasrechner_asti_out, lc_glasrechner_vz_out;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.glasberechnung);

        // Verhindert das Öffnen des Tastaturlayouts beim Starten der Activity
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        et_glasrechner_sph_in = (EditText) findViewById(R.id.et_glasrechner_sph_in);
        et_glasrechner_sph_in.requestFocus();

        cb_glasrechner_werkzeugindex = (CheckBox) findViewById(R.id.cb_glasrechner_werkzeugindex);
        cb_glasrechner_flaechen_gerundet =
                (CheckBox) findViewById(R.id.cb_glasrechner_flaechen_gerundet);
        cb_glasrechner_flaechen_gerundet.setEnabled(false);

        cb_glasrechner_werkzeugindex.setOnCheckedChangeListener
                (new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (cb_glasrechner_werkzeugindex.isChecked()) {
                    cb_glasrechner_flaechen_gerundet.setEnabled(true);
                } else {
                    cb_glasrechner_flaechen_gerundet.setChecked(false);
                    cb_glasrechner_flaechen_gerundet.setEnabled(false);
                }
            }
        });

        bt = (Button) findViewById(R.id.bT);
        bt.setOnClickListener(new View.OnClickListener() {
           public void onClick(View v) {

               txt_HS2_wunsch = (TextView) findViewById(R.id.txt_HS2_wunsch);
               txt_HS2_wunsch.setVisibility(View.GONE);
               txt_glasrechner_hs2_out_einheit = (TextView)
                       findViewById(R.id.txt_glasrechner_hs2_out_einheit);
               txt_glasrechner_hs2_out_einheit.setVisibility(View.GONE);
               txt_F1_real = (TextView) findViewById(R.id.txt_F1_real);
               txt_F2_real = (TextView) findViewById(R.id.txt_F2_real);
               txt_F2_wkn = (TextView) findViewById(R.id.txt_F2_wkn);
               txt_F2_wkn.setVisibility(View.GONE);
               txt_glasrechner_f2_wkn_out_einheit = (TextView)
                       findViewById(R.id.txt_glasrechner_f2_wkn_out_einheit);
               txt_glasrechner_f2_wkn_out_einheit.setVisibility(View.GONE);
               txt_F3_wkn = (TextView) findViewById(R.id.txt_F3_wkn);
               txt_F3_wkn.setVisibility(View.GONE);
               txt_glasrechner_f3_wkn_out_einheit = (TextView)
                       findViewById(R.id.txt_glasrechner_f3_wkn_out_einheit);
               txt_glasrechner_f3_wkn_out_einheit.setVisibility(View.GONE);
               txt_F3_real = (TextView) findViewById(R.id.txt_F3_real);
               txt_F3_real.setVisibility(View.GONE);
               txt_glasrechner_f3_real_out_einheit = (TextView)
                       findViewById(R.id.txt_glasrechner_f3_real_out_einheit);
               txt_glasrechner_f3_real_out_einheit.setVisibility(View.GONE);
               txt_R3 = (TextView) findViewById(R.id.txt_R3);
               txt_R3.setVisibility(View.GONE);
               txt_glasrechner_r3_out_einheit = (TextView)
                       findViewById(R.id.txt_glasrechner_r3_out_einheit);
               txt_glasrechner_r3_out_einheit.setVisibility(View.GONE);
               txt_DR3 = (TextView) findViewById(R.id.txt_DR3);
               txt_DR3.setVisibility(View.GONE);
               txt_glasrechner_dr3_out_einheit = (TextView)
                       findViewById(R.id.txt_glasrechner_dr3_out_einheit);
               txt_glasrechner_dr3_out_einheit.setVisibility(View.GONE);
               txt_glasrechner_hs2_refra = (TextView) findViewById(R.id.txt_glasrechner_hs2_refra);
               txt_glasrechner_hs2_refra.setVisibility(View.GONE);
               txt_glasrechner_hs2_asti = (TextView) findViewById(R.id.txt_glasrechner_hs2_asti);
               txt_glasrechner_hs2_asti.setVisibility(View.GONE);
               txt_glasrechner_hs2_vz = (TextView) findViewById(R.id.txt_glasrechner_hs2_vz);
               txt_glasrechner_hs2_vz.setVisibility(View.GONE);

               txt_glasrechner_hs2_out = (TextView) findViewById(R.id.txt_glasrechner_hs2_out);
               txt_glasrechner_hs2_out.setVisibility(View.GONE);
               txt_glasrechner_f2_wkn_out = (TextView)
                       findViewById(R.id.txt_glasrechner_f2_wkn_out);
               txt_glasrechner_f2_wkn_out.setVisibility(View.GONE);
               txt_glasrechner_f3_wkn_out = (TextView)
                       findViewById(R.id.txt_glasrechner_f3_wkn_out);
               txt_glasrechner_f3_wkn_out.setVisibility(View.GONE);
               txt_glasrechner_f3_real_out = (TextView)
                       findViewById(R.id.txt_glasrechner_f3_real_out);
               txt_glasrechner_f3_real_out.setVisibility(View.GONE);
               txt_glasrechner_r3_out = (TextView)
                       findViewById(R.id.txt_glasrechner_r3_out);
               txt_glasrechner_r3_out.setVisibility(View.GONE);
               txt_glasrechner_dr3_out = (TextView) findViewById(R.id.txt_glasrechner_dr3_out);
               txt_glasrechner_dr3_out.setVisibility(View.GONE);
               txt_glasrechner_refra_hs2_out = (TextView)
                       findViewById(R.id.txt_glasrechner_refra_hs2_out);
               txt_glasrechner_refra_hs2_out.setVisibility(View.GONE);
               txt_glasrechner_asti_hs2_out = (TextView)
                       findViewById(R.id.txt_glasrechner_asti_hs2_out);
               txt_glasrechner_asti_hs2_out.setVisibility(View.GONE);
               txt_glasrechner_vz_hs2_out = (TextView)
                       findViewById(R.id.txt_glasrechner_vz_hs2_out);
               txt_glasrechner_vz_hs2_out.setVisibility(View.GONE);


               txt_glasrechner_hs1_out = (TextView) findViewById(R.id.txt_glasrechner_hs1_out);
               txt_glasrechner_f1_real_out = (TextView)
                       findViewById(R.id.txt_glasrechner_f1_real_out);
               txt_glasrechner_f2_real_out = (TextView)
                       findViewById(R.id.txt_glasrechner_f2_real_out);
               txt_glasrechner_r1_out = (TextView) findViewById(R.id.txt_glasrechner_r1_out);
               txt_glasrechner_r2_out = (TextView) findViewById(R.id.txt_glasrechner_r2_out);
               txt_glasrechner_dr2_out = (TextView) findViewById(R.id.txt_glasrechner_dr2_out);
               txt_glasrechner_bauhoehe_out = (TextView)
                       findViewById(R.id.txt_glasrechner_bauhoehe_out);
               txt_glasrechner_volumen_out = (TextView)
                       findViewById(R.id.txt_glasrechner_volumen_out);
               txt_glasrechner_gewicht_out = (TextView)
                       findViewById(R.id.txt_glasrechner_gewicht_out);
               txt_glasrechner_grad_refra_out = (TextView)
                       findViewById(R.id.txt_glasrechner_grad_refra_out);
               txt_glasrechner_refra_hs1_out = (TextView)
                       findViewById(R.id.txt_glasrechner_refra_hs1_out);
               txt_glasrechner_grad_asti_out = (TextView)
                       findViewById(R.id.txt_glasrechner_grad_asti_out);
               txt_glasrechner_asti_hs1_out = (TextView)
                       findViewById(R.id.txt_glasrechner_asti_hs1_out);
               txt_glasrechner_grad_vz_out = (TextView)
                       findViewById(R.id.txt_glasrechner_grad_vz_out);
               txt_glasrechner_vz_hs1_out = (TextView)
                       findViewById(R.id.txt_glasrechner_vz_hs1_out);

               lc_glasrechner_refra_out = (LineChart) findViewById(R.id.lc_glasrechner_refra_out);
               lc_glasrechner_asti_out = (LineChart) findViewById(R.id.lc_glasrechner_asti_out);
               lc_glasrechner_vz_out = (LineChart) findViewById(R.id.lc_glasrechner_vz_out);


               et_glasrechner_cyl_in = (EditText) findViewById(R.id.et_glasrechner_cyl_in);
               et_glasrechner_f1_in = (EditText) findViewById(R.id.et_glasrechner_f1_in);
               et_glasrechner_d_in = (EditText) findViewById(R.id.et_glasrechner_d_in);
               et_glasrechner_dr_in = (EditText) findViewById(R.id.et_glasrechner_dr_in);
               et_glasrechner_n_in = (EditText) findViewById(R.id.et_glasrechner_n_in);
               et_glasrechner_durchmesser_in = (EditText)
                       findViewById(R.id.et_glasrechner_durchmesser_in);
               et_glasrechner_rho_in = (EditText) findViewById(R.id.et_glasrechner_rho_in);
               et_glasrechner_b_1_in = (EditText) findViewById(R.id.et_glasrechner_b_1_in);
               et_glasrechner_s_1_in = (EditText) findViewById(R.id.et_glasrechner_s_1_in);
               et_glasrechner_blickwinkel_in =(EditText)
                       findViewById(R.id.et_glasrechner_blickwinkel_in);
               et_glasrechner_abstufung_in = (EditText)
                       findViewById(R.id.et_glasrechner_abstufung_in);

               grid_glasrechner_result = (GridLayout) findViewById(R.id.grid_glasrechner_result);

               String Alarm = et_glasrechner_sph_in.getText().toString();
               if(Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") || Alarm.equals("+")) {
                   et_glasrechner_sph_in.setText("0");
               }

               double sph_in = Double.parseDouble(et_glasrechner_sph_in.getText().toString());

               Alarm = et_glasrechner_cyl_in.getText().toString();
               if(Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") || Alarm.equals("+")) {
                   et_glasrechner_cyl_in.setText("0");
               }

               double cyl_in = Double.parseDouble(et_glasrechner_cyl_in.getText().toString());

               Alarm = et_glasrechner_f1_in.getText().toString();
               if(Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") || Alarm.equals("+")) {
                   double fix;
                   fix = 4;
                   if (sph_in > 0 || sph_in+cyl_in > 0){
                       if (cyl_in > 0) {
                           fix = sph_in + cyl_in + 1;
                       } else {
                           fix = sph_in + 1;
                       }

                   }
                   et_glasrechner_f1_in.setText(Double.toString(Math.round((fix)*100)/100.0));
               }

               double f1_in = Double.parseDouble(et_glasrechner_f1_in.getText().toString());

               Alarm = et_glasrechner_n_in.getText().toString();
               if(Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") || Alarm.equals("+")) {
                   AlertDialog.Builder AlarmBox = new AlertDialog.Builder(context);
                   AlarmBox.setTitle("Leeres Feld!");
                   AlarmBox.setMessage("n ist leer");
                   AlarmBox.setNeutralButton("OK", null);

                   AlertDialog dialog = AlarmBox.create();
                   dialog.show();
                   return;
               }

               double n_in = Double.parseDouble(et_glasrechner_n_in.getText().toString());

               Alarm = et_glasrechner_d_in.getText().toString();
               if((Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") || Alarm.equals("+")
                       || Alarm.equals("0")) && ((sph_in+cyl_in < 0) && (sph_in < 0))) {
                   AlertDialog.Builder AlarmBox = new AlertDialog.Builder(context);
                   AlarmBox.setTitle("Leeres Feld!");
                   AlarmBox.setMessage("Wenn beide HS negativ sind muss die Mittenddicke d " +
                           "angegeben werden");
                   AlarmBox.setNeutralButton("OK", null);

                   AlertDialog dialog = AlarmBox.create();
                   dialog.show();
                   return;
               } else if (Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") ||
                       Alarm.equals("+")) {
                   et_glasrechner_d_in.setText("1.2");
               }

               double d_in = Double.parseDouble(et_glasrechner_d_in.getText().toString());

               Alarm = et_glasrechner_dr_in.getText().toString();
               if((Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") || Alarm.equals("+"))
                       && ((sph_in+cyl_in >= 0) || (sph_in >= 0))) {
                   AlertDialog.Builder AlarmBox = new AlertDialog.Builder(context);
                   AlarmBox.setTitle("Leeres Feld!");
                   AlarmBox.setMessage(Html.fromHtml("Wenn min. ein HS positiv ist muss die min. " +
                           "Randdicke d<sub><small>r</small></sub> angegeben werden <br>"));
                   AlarmBox.setNeutralButton("OK", null);

                   AlertDialog dialog = AlarmBox.create();
                   dialog.show();
                   return;
               } else if (Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") ||
                       Alarm.equals("+")) {
                   et_glasrechner_dr_in.setText("0.7");
               }

               double dr_in = Double.parseDouble(et_glasrechner_dr_in.getText().toString());

               Alarm = et_glasrechner_durchmesser_in.getText().toString();
               if(Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") || Alarm.equals("+")
                       || Alarm.equals("0")) {
                   et_glasrechner_durchmesser_in.setText("60");
               }

               double durchmesser_in =
                       Double.parseDouble(et_glasrechner_durchmesser_in.getText().toString());

               Alarm = et_glasrechner_rho_in.getText().toString();
               if(Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") || Alarm.equals("+")) {
                   et_glasrechner_rho_in.setText("1.2");
               }

               double rho_in = Double.parseDouble(et_glasrechner_rho_in.getText().toString());

               Alarm = et_glasrechner_b_1_in.getText().toString();
               if(Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") || Alarm.equals("+") ||
                       Alarm.equals("0")) {
                   et_glasrechner_b_1_in.setText("27.5");
               }

               double b_1_in = Double.parseDouble(et_glasrechner_b_1_in.getText().toString());

               Alarm = et_glasrechner_s_1_in.getText().toString();
               if(Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") || Alarm.equals("+") ||
                       Alarm.equals("0")) {
                   et_glasrechner_s_1_in.setText("-1000");
               }

               double s_1_in = Double.parseDouble(et_glasrechner_s_1_in.getText().toString());

               Alarm = et_glasrechner_blickwinkel_in.getText().toString();
               if(Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") || Alarm.equals("+") ||
                       Alarm.equals("0")) {
                   et_glasrechner_blickwinkel_in.setText("1");
               }

               int blickwinkel_in =
                       Integer.parseInt(et_glasrechner_blickwinkel_in.getText().toString());

               Alarm = et_glasrechner_abstufung_in.getText().toString();
               if(Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") || Alarm.equals("+") ||
                       Alarm.equals("0")) {
                   et_glasrechner_abstufung_in.setText("1");
               }

               int abstufung_in =
                       Integer.parseInt(et_glasrechner_abstufung_in.getText().toString());
               for (int i=0; abstufung_in > blickwinkel_in; i++) {
                   abstufung_in = abstufung_in - 1;
               }
               et_glasrechner_abstufung_in.setText(Integer.toString(abstufung_in));

               grid_glasrechner_result.setVisibility(View.VISIBLE);


               double f1_real, f2_real, f2_wkn, f3_real, f3_wkn;
               double hs1, hs2;
               double r1, r2, r3;

               double dr, ddr;
               double t1, t2, t3, dr1, dr2;
               r1 = 100;
               r2 = 100;
               r3 = 100;

            if (sph_in >= 0 || sph_in+cyl_in >= 0) {

                for (int i = 0; i < 4; i++) {

                    if (cb_glasrechner_werkzeugindex.isChecked()) {
                        if (cb_glasrechner_flaechen_gerundet.isChecked()) {
                            f1_real = ((n_in - 1) / (1.525 - 1)) * f1_in;
                            f2_real = (sph_in - (f1_real / (1 - ((d_in * 0.001) / n_in) *
                                    f1_real)));
                            f2_wkn = Math.round((((1.525 - 1) / (n_in - 1)) * f2_real) / 0.0625) *
                                    0.0625;
                            f2_real = ((n_in - 1) / (1.525 - 1)) * f2_wkn;

                            txt_glasrechner_f1_real_out.setText(
                                    Double.toString(Math.round(f1_real * 10000) / 10000.0));
                            txt_F1_real.setText(Html.fromHtml("F<sub><small>1<sub><small>"+
                                    Double.toString(Math.round(n_in*1000)/1000.0)+
                                    "</small></sub></small></sub>:"));
                            txt_glasrechner_f2_real_out.setText(
                                    Double.toString(Math.round(f2_real * 10000) / 10000.0));
                            txt_F2_real.setText(Html.fromHtml("F<sub><small>2<sub><small>"+
                                    Double.toString(Math.round(n_in*1000)/1000.0)+
                                    "</small></sub></small></sub>:"));
                            txt_glasrechner_f2_wkn_out.setText(Double.toString(f2_wkn));

                            hs1 = f2_real + (f1_real / (1 - ((d_in * 0.001) / n_in) * f1_real));

                            txt_glasrechner_hs1_out.setText(
                                    Double.toString(Math.round(hs1 * 10000) / 10000.0));

                            r1 = 1000 * (n_in - 1) / f1_real;
                            r2 = 1000 * (1 - n_in) / f2_real;

                            txt_glasrechner_r1_out.setText(
                                    Double.toString(Math.round(r1 * 10000) / 10000.0));
                            txt_glasrechner_r2_out.setText(
                                    Double.toString(Math.round(r2 * 10000) / 10000.0));

                            if (cyl_in != 0) {
                                f3_real = ((sph_in + cyl_in) -
                                        (f1_real / (1 - ((d_in * 0.001) / n_in) * f1_real)));
                                f3_wkn = Math.round((((1.525 - 1) / (n_in - 1)) * f3_real) / 0.0625)
                                        * 0.0625;
                                f3_real = ((n_in - 1) / (1.525 - 1)) * f3_wkn;

                                txt_glasrechner_f3_real_out.setText(
                                        Double.toString(Math.round(f3_real * 10000) / 10000.0));
                                txt_F3_real.setText(Html.fromHtml("F<sub><small>3<sub><small>"+
                                        Double.toString(Math.round(n_in*1000)/1000.0)+
                                        "</small></sub></small></sub>:"));
                                txt_glasrechner_f3_wkn_out.setText(Double.toString(f3_wkn));

                                hs2 = f3_real + (f1_real / (1 - ((d_in * 0.001) / n_in) * f1_real));

                                txt_glasrechner_hs2_out.setText(Double.toString(
                                        Math.round(hs2 * 10000) / 10000.0));

                                r3 = 1000 * (1 - n_in) / f3_real;

                                txt_glasrechner_r3_out.setText(Double.toString(
                                        Math.round(r3 * 10000) / 10000.0));
                            }
                        } else {
                            f1_real = ((n_in - 1) / (1.525 - 1)) * f1_in;
                            f2_real = (sph_in - (f1_real / (1 - ((d_in * 0.001) / n_in) *
                                    f1_real)));
                            f2_wkn = (((1.525 - 1) / (n_in - 1)) * f2_real);


                            txt_glasrechner_f1_real_out.setText(
                                    Double.toString(Math.round(f1_real * 10000) / 10000.0));
                            txt_F1_real.setText(Html.fromHtml("F<sub><small>1<sub><small>"+
                                    Double.toString(Math.round(n_in*1000)/1000.0)+
                                    "</small></sub></small></sub>:"));
                            txt_glasrechner_f2_real_out.setText(
                                    Double.toString(Math.round(f2_real * 10000) / 10000.0));
                            txt_F2_real.setText(Html.fromHtml("F<sub><small>2<sub><small>"+
                                    Double.toString(Math.round(n_in*1000)/1000.0)+
                                    "</small></sub></small></sub>:"));
                            txt_glasrechner_f2_wkn_out.setText(
                                    Double.toString(Math.round(f2_wkn * 10000) / 10000.0));

                            hs1 = f2_real + (f1_real / (1 - ((d_in * 0.001) / n_in) * f1_real));

                            txt_glasrechner_hs1_out.setText(
                                    Double.toString(Math.round(hs1 * 10000) / 10000.0));

                            r1 = 1000 * (n_in - 1) / f1_real;
                            r2 = 1000 * (1 - n_in) / f2_real;

                            txt_glasrechner_r1_out.setText(
                                    Double.toString(Math.round(r1 * 10000) / 10000.0));
                            txt_glasrechner_r2_out.setText(
                                    Double.toString(Math.round(r2 * 10000) / 10000.0));

                            if (cyl_in != 0) {
                                f3_real = ((sph_in + cyl_in) - (f1_real / (1 - ((d_in * 0.001) /
                                        n_in) * f1_real)));
                                f3_wkn = (((1.525 - 1) / (n_in - 1)) * f3_real);


                                txt_glasrechner_f3_real_out.setText(
                                        Double.toString(Math.round(f3_real * 10000) / 10000.0));
                                txt_F3_real.setText(Html.fromHtml("F<sub><small>3<sub><small>"+
                                        Double.toString(Math.round(n_in*1000)/1000.0)+
                                        "</small></sub></small></sub>:"));
                                txt_glasrechner_f3_wkn_out.setText(
                                        Double.toString(Math.round(f3_wkn * 10000) / 10000.0));

                                hs2 = f3_real + (f1_real / (1 - ((d_in * 0.001) / n_in) * f1_real));

                                txt_glasrechner_hs2_out.setText(
                                        Double.toString(Math.round(hs2 * 10000) / 10000.0));

                                r3 = 1000 * (1 - n_in) / f3_real;

                                txt_glasrechner_r3_out.setText(
                                        Double.toString(Math.round(r3 * 10000) / 10000.0));
                            }
                        }
                    } else {
                        f1_real = ((n_in - 1) / (1.525 - 1)) * f1_in;
                        f2_real = (sph_in - (f1_real / (1 - ((d_in * 0.001) / n_in) * f1_real)));

                        txt_glasrechner_f1_real_out.setText(
                                Double.toString(Math.round(f1_real * 10000) / 10000.0));
                        txt_F1_real.setText(Html.fromHtml("F<sub><small>1<sub><small>"+
                                Double.toString(Math.round(n_in*1000)/1000.0)+
                                "</small></sub></small></sub>:"));
                        txt_glasrechner_f2_real_out.setText(
                                Double.toString(Math.round(f2_real * 10000) / 10000.0));
                        txt_F2_real.setText(Html.fromHtml("F<sub><small>2<sub><small>"+
                                Double.toString(Math.round(n_in*1000)/1000.0)+
                                "</small></sub></small></sub>:"));

                        hs1 = f2_real + (f1_real / (1 - ((d_in * 0.001) / n_in) * f1_real));

                        txt_glasrechner_hs1_out.setText(
                                Double.toString(Math.round(hs1 * 10000) / 10000.0));

                        r1 = 1000 * (n_in - 1) / f1_real;
                        r2 = 1000 * (1 - n_in) / f2_real;

                        txt_glasrechner_r1_out.setText(
                                Double.toString(Math.round(r1 * 10000) / 10000.0));
                        txt_glasrechner_r2_out.setText(
                                Double.toString(Math.round(r2 * 10000) / 10000.0));

                        if (cyl_in != 0) {
                            f3_real = ((sph_in + cyl_in) - (f1_real / (1 - ((d_in * 0.001) / n_in) *
                                    f1_real)));

                            txt_glasrechner_f3_real_out.setText(
                                    Double.toString(Math.round(f3_real * 10000) / 10000.0));
                            txt_F3_real.setText(Html.fromHtml("F<sub><small>3<sub><small>"+
                                    Double.toString(Math.round(n_in*1000)/1000.0)+
                                    "</small></sub></small></sub>:"));

                            hs2 = f3_real + (f1_real / (1 - ((d_in * 0.001) / n_in) * f1_real));

                            txt_glasrechner_hs2_out.setText(
                                    Double.toString(Math.round(hs2 * 10000) / 10000.0));

                            r3 = 1000 * (1 - n_in) / f3_real;

                            txt_glasrechner_r3_out.setText(
                                    Double.toString(Math.round(r3 * 10000) / 10000.0));
                        }
                    }

                    t1 = r1 * (1 - Math.sqrt(1 - ((Math.pow((durchmesser_in / 2), 2) /
                            (Math.pow(r1, 2))))));
                    t2 = r2 * (1 - Math.sqrt(1 - ((Math.pow((durchmesser_in / 2), 2) /
                            (Math.pow(r2, 2))))));

                    if (cyl_in != 0) {

                        t3 = r3 * (1 - Math.sqrt(1 - ((Math.pow((durchmesser_in / 2), 2) /
                                (Math.pow(r3, 2))))));

                        if (r2 >= r3) {

                            dr = d_in - t1 + t2;
                            ddr = dr - dr_in;
                            d_in = d_in - ddr;
                            d_in = Math.round(d_in*10000)/10000.0;
                        } else {
                            dr = d_in - t1 + t3;
                            ddr = dr - dr_in;
                            d_in = d_in - ddr;
                            d_in = Math.round(d_in*10000)/10000.0;
                        }
                    } else {
                        dr = d_in - t1 + t2;
                        ddr = dr - dr_in;
                        d_in = d_in - ddr;
                        d_in = Math.round(d_in*10000)/10000.0;
                    }

                    et_glasrechner_d_in.setText(Double.toString(d_in));

                }
            } else {
                if (cb_glasrechner_werkzeugindex.isChecked()) {
                    if (cb_glasrechner_flaechen_gerundet.isChecked()) {
                        f1_real = ((n_in - 1) / (1.525 - 1)) * f1_in;
                        f2_real = (sph_in - (f1_real / (1 - ((d_in * 0.001) / n_in) * f1_real)));
                        f2_wkn = Math.round((((1.525 - 1) / (n_in - 1)) * f2_real) / 0.0625) *
                                0.0625;
                        f2_real = ((n_in - 1) / (1.525 - 1)) * f2_wkn;

                        txt_glasrechner_f1_real_out.setText(
                                Double.toString(Math.round(f1_real * 10000) / 10000.0));
                        txt_F1_real.setText(Html.fromHtml("F<sub><small>1<sub><small>"+
                                Double.toString(Math.round(n_in*1000)/1000.0)+
                                "</small></sub></small></sub>:"));
                        txt_glasrechner_f2_real_out.setText(
                                Double.toString(Math.round(f2_real * 10000) / 10000.0));
                        txt_F2_real.setText(Html.fromHtml("F<sub><small>2<sub><small>"+
                                Double.toString(Math.round(n_in*1000)/1000.0)+
                                "</small></sub></small></sub>:"));
                        txt_glasrechner_f2_wkn_out.setText(Double.toString(f2_wkn));

                        hs1 = f2_real + (f1_real / (1 - ((d_in * 0.001) / n_in) * f1_real));

                        txt_glasrechner_hs1_out.setText(
                                Double.toString(Math.round(hs1 * 10000) / 10000.0));

                        r1 = 1000 * (n_in - 1) / f1_real;
                        r2 = 1000 * (1 - n_in) / f2_real;

                        txt_glasrechner_r1_out.setText(
                                Double.toString(Math.round(r1 * 10000) / 10000.0));
                        txt_glasrechner_r2_out.setText(
                                Double.toString(Math.round(r2 * 10000) / 10000.0));

                        if (cyl_in != 0) {
                            f3_real = ((sph_in + cyl_in) - (f1_real / (1 - ((d_in * 0.001) /
                                    n_in) * f1_real)));
                            f3_wkn = Math.round((((1.525 - 1) / (n_in - 1)) * f3_real) / 0.0625) *
                                    0.0625;
                            f3_real = ((n_in - 1) / (1.525 - 1)) * f3_wkn;

                            txt_glasrechner_f3_real_out.setText(
                                    Double.toString(Math.round(f3_real * 10000) / 10000.0));
                            txt_F3_real.setText(Html.fromHtml("F<sub><small>3<sub><small>"+
                                    Double.toString(Math.round(n_in*1000)/1000.0)+
                                    "</small></sub></small></sub>:"));
                            txt_glasrechner_f3_wkn_out.setText(Double.toString(f3_wkn));

                            hs2 = f3_real + (f1_real / (1 - ((d_in * 0.001) / n_in) * f1_real));

                            txt_glasrechner_hs2_out.setText(
                                    Double.toString(Math.round(hs2 * 10000) / 10000.0));

                            r3 = 1000 * (1 - n_in) / f3_real;

                            txt_glasrechner_r3_out.setText(
                                    Double.toString(Math.round(r3 * 10000) / 10000.0));
                        }
                    } else {
                        f1_real = ((n_in - 1) / (1.525 - 1)) * f1_in;
                        f2_real = (sph_in - (f1_real / (1 - ((d_in * 0.001) / n_in) * f1_real)));
                        f2_wkn = (((1.525 - 1) / (n_in - 1)) * f2_real);


                        txt_glasrechner_f1_real_out.setText(
                                Double.toString(Math.round(f1_real * 10000) / 10000.0));
                        txt_F1_real.setText(Html.fromHtml("F<sub><small>1<sub><small>"+
                                Double.toString(Math.round(n_in*1000)/1000.0)+
                                "</small></sub></small></sub>:"));
                        txt_glasrechner_f2_real_out.setText(
                                Double.toString(Math.round(f2_real * 10000) / 10000.0));
                        txt_F2_real.setText(Html.fromHtml("F<sub><small>2<sub><small>"+
                                Double.toString(Math.round(n_in*1000)/1000.0)+
                                "</small></sub></small></sub>:"));
                        txt_glasrechner_f2_wkn_out.setText(
                                Double.toString(Math.round(f2_wkn * 10000) / 10000.0));

                        hs1 = f2_real + (f1_real / (1 - ((d_in * 0.001) / n_in) * f1_real));

                        txt_glasrechner_hs1_out.setText(
                                Double.toString(Math.round(hs1 * 10000) / 10000.0));

                        r1 = 1000 * (n_in - 1) / f1_real;
                        r2 = 1000 * (1 - n_in) / f2_real;

                        txt_glasrechner_r1_out.setText(
                                Double.toString(Math.round(r1 * 10000) / 10000.0));
                        txt_glasrechner_r2_out.setText(
                                Double.toString(Math.round(r2 * 10000) / 10000.0));

                        if (cyl_in != 0) {
                            f3_real = ((sph_in + cyl_in) - (f1_real / (1 - ((d_in * 0.001) /
                                    n_in) * f1_real)));
                            f3_wkn = (((1.525 - 1) / (n_in - 1)) * f3_real);


                            txt_glasrechner_f3_real_out.setText(
                                    Double.toString(Math.round(f3_real * 10000) / 10000.0));
                            txt_F3_real.setText(Html.fromHtml("F<sub><small>3<sub><small>"+
                                    Double.toString(Math.round(n_in*1000)/1000.0)+
                                    "</small></sub></small></sub>:"));
                            txt_glasrechner_f3_wkn_out.setText(
                                    Double.toString(Math.round(f3_wkn * 10000) / 10000.0));

                            hs2 = f3_real + (f1_real / (1 - ((d_in * 0.001) / n_in) * f1_real));

                            txt_glasrechner_hs2_out.setText(
                                    Double.toString(Math.round(hs2 * 10000) / 10000.0));

                            r3 = 1000 * (1 - n_in) / f3_real;

                            txt_glasrechner_r3_out.setText(
                                    Double.toString(Math.round(r3 * 10000) / 10000.0));
                        }
                    }
                } else {
                    f1_real = ((n_in - 1) / (1.525 - 1)) * f1_in;
                    f2_real = (sph_in - (f1_real / (1 - ((d_in * 0.001) / n_in) * f1_real)));

                    txt_glasrechner_f1_real_out.setText(
                            Double.toString(Math.round(f1_real * 10000) / 10000.0));
                    txt_F1_real.setText(Html.fromHtml("F<sub><small>1<sub><small>"+
                            Double.toString(Math.round(n_in*1000)/1000.0)+
                            "</small></sub></small></sub>:"));
                    txt_glasrechner_f2_real_out.setText(
                            Double.toString(Math.round(f2_real * 10000) / 10000.0));
                    txt_F2_real.setText(Html.fromHtml("F<sub><small>2<sub><small>"+
                            Double.toString(Math.round(n_in*1000)/1000.0)+
                            "</small></sub></small></sub>:"));

                    hs1 = f2_real + (f1_real / (1 - ((d_in * 0.001) / n_in) * f1_real));

                    txt_glasrechner_hs1_out.setText(
                            Double.toString(Math.round(hs1 * 10000) / 10000.0));

                    r1 = 1000 * (n_in - 1) / f1_real;
                    r2 = 1000 * (1 - n_in) / f2_real;

                    txt_glasrechner_r1_out.setText(
                            Double.toString(Math.round(r1 * 10000) / 10000.0));
                    txt_glasrechner_r2_out.setText(
                            Double.toString(Math.round(r2 * 10000) / 10000.0));

                    if (cyl_in != 0) {
                        f3_real = ((sph_in + cyl_in) - (f1_real / (1 - ((d_in * 0.001) / n_in) *
                                f1_real)));

                        txt_glasrechner_f3_real_out.setText(
                                Double.toString(Math.round(f3_real * 10000) / 10000.0));
                        txt_F3_real.setText(Html.fromHtml("F<sub><small>3<sub><small>"+
                                Double.toString(Math.round(n_in*1000)/1000.0)+
                                "</small></sub></small></sub>:"));

                        hs2 = f3_real + (f1_real / (1 - ((d_in * 0.001) / n_in) * f1_real));

                        txt_glasrechner_hs2_out.setText(
                                Double.toString(Math.round(hs2 * 10000) / 10000.0));

                        r3 = 1000 * (1 - n_in) / f3_real;

                        txt_glasrechner_r3_out.setText(
                                Double.toString(Math.round(r3 * 10000) / 10000.0));
                    }
                }
            }

               ArrayList<Entry> yAXES_refra_hs1 = new ArrayList<>();
               ArrayList<Entry> yAXES_refra_hs2 = new ArrayList<>();
               ArrayList<Entry> yAXES_asti_hs1 = new ArrayList<>();
               ArrayList<Entry> yAXES_asti_hs2 = new ArrayList<>();
               ArrayList<Entry> yAXES_vz_hs1 = new ArrayList<>();
               ArrayList<Entry> yAXES_vz_hs2 = new ArrayList<>();

               double s_2, u_2_rad, Sin_e_2, Sin_e2, e_2, e2, u2, s2, alpha2,
                       s_1, u_1, Sin_e_1, e_1, Sin_e1, e1, alpha1, u1;
               double l_t1, Ast_Konst_1, Hilfe1, l_t_1, L_t_1, l_s1, Hilfe2, l_s_1;
               double p1, p2, ds;
               double l_t2, Ast_Konst_2, Hilfe21, l_t_2, l_s2, Hilfe22, l_s_2;
               double delta_s_1, t_sk_1, s_sk_1, T_sk_1, S_sk_1, mitt_Brechkraft, Astigmatismus,
                       Refraktionsfehler, Verzeichnung;

               double u1_absolut;

               int Grad;

               String Grad_Anzeige = "";
               String hs1_refra_list, hs1_asti_list, hs1_vz_list;
               String hs2_refra_list, hs2_asti_list, hs2_vz_list;

               String Anhang;

               hs1_asti_list = "";
               hs1_refra_list = "";
               hs1_vz_list = "";

               hs2_asti_list = "";
               hs2_refra_list = "";
               hs2_vz_list = "";

               u1_absolut = 0.001;
               double u1_absolut2 = 0.001;

               Grad = 0;
               u_2_rad = Math.toRadians(0.001);

               for (int i=0; Grad <= blickwinkel_in; i++) {
                   Grad_Anzeige = Grad_Anzeige + Integer.toString(Grad) + " ° <br>";
                   s_2 = b_1_in;
                   Sin_e_2 = (s_2-r2)/r2*Math.sin(u_2_rad);
                   Sin_e2 = (1/n_in)*Sin_e_2;
                   e_2 = Math.asin(Sin_e_2);
                   e2 = Math.asin(Sin_e2);
                   u2 = u_2_rad+e_2-e2;
                   s2 = r2*(1+(Sin_e2/Math.sin(u2)));
                   alpha2 = u_2_rad+e_2;

                   s_1 = d_in+s2;
                   u_1 = u2;
                   Sin_e_1 = (s_1-r1)/r1*Math.sin(u_1);
                   e_1 = Math.asin(Sin_e_1);
                   Sin_e1 = (n_in/1)*Sin_e_1;
                   e1 = Math.asin(Sin_e1);
                   alpha1 = u_1+e_1;
                   u1 = u_1+e_1-e1;
                   if (Grad == 0) {
                       u1_absolut = u1;
                   }

                   l_t1 = s_1_in*1000;
                   Ast_Konst_1 = (n_in*Math.cos(e_1)-1*Math.cos(e1))/r1;
                   Hilfe1 = 1*Math.cos(e1)*Math.cos(e1)/l_t1;
                   l_t_1 = n_in*Math.cos(e_1)*Math.cos(e_1)/(Ast_Konst_1+Hilfe1);

                   l_s1 = l_t1;
                   Hilfe2 = 1/l_s1;
                   l_s_1 = n_in/(Ast_Konst_1+Hilfe2);

                   p1 = r1*(1-Math.cos(alpha1));
                   p2 = r2*(1-Math.cos(alpha2));
                   ds = (d_in+p2-p1)/Math.cos(u2);

                   l_t2 = l_t_1-ds;
                   Ast_Konst_2 = (1*Math.cos(e_2)-n_in*Math.cos(e2))/r2;
                   Hilfe21 = n_in*Math.cos(e2)*Math.cos(e2)/l_t2;
                   l_t_2 = 1*Math.cos(e_2)*Math.cos(e_2)/(Ast_Konst_2+Hilfe21);

                   l_s2 = l_s_1-ds;
                   Hilfe22 = n_in/l_s2;
                   l_s_2 = 1/(Ast_Konst_2+Hilfe22);

                   delta_s_1 = (r2*(Math.sin(alpha2)/Math.sin(u_2_rad)))-b_1_in;

                   t_sk_1 = l_t_2-delta_s_1;
                   s_sk_1 = l_s_2-delta_s_1;
                   T_sk_1 = 1000/t_sk_1;
                   S_sk_1 = 1000/s_sk_1;
                   mitt_Brechkraft = 0.5*(T_sk_1+S_sk_1);
                   Astigmatismus = T_sk_1-S_sk_1;
                   Anhang = Double.toString(Math.round(Astigmatismus*10000)/10000.0);
                   if (Anhang.length()>10) {
                       Anhang = String.format("%6.4e", Astigmatismus).replace(",",".");
                   }
                   hs1_asti_list = hs1_asti_list + Anhang + " dpt<br>";

                   float y_data_asti_hs1 = Float.parseFloat(String.valueOf(Astigmatismus));
                   yAXES_asti_hs1.add(new Entry(Grad,y_data_asti_hs1));

                   Refraktionsfehler = mitt_Brechkraft-sph_in-1000/l_t1;
                   Anhang = Double.toString(Math.round(Refraktionsfehler*10000)/10000.0);
                   if (Anhang.length()>10) {
                       Anhang = String.format("%6.4e", Refraktionsfehler).replace(",",".");
                   }
                   hs1_refra_list = hs1_refra_list + Anhang + " dpt<br>";

                   float y_data_refra_hs1 = Float.parseFloat(String.valueOf(Refraktionsfehler));
                   yAXES_refra_hs1.add(new Entry(Grad,y_data_refra_hs1));

                   Verzeichnung = (Math.tan(u_2_rad)/Math.tan(u1))/(Math.tan(Math.toRadians(0.001))/
                           Math.tan(u1_absolut))*100-100;
                   Anhang = Double.toString(Math.round(Verzeichnung*100)/100.0);
                   if (Anhang.length()>10) {
                       Anhang = String.format("%6.4e", Verzeichnung).replace(",",".");
                   }
                   hs1_vz_list = hs1_vz_list + Anhang + " %<br>";

                   float y_data_vz_hs1 = Float.parseFloat(String.valueOf(Verzeichnung));
                   yAXES_vz_hs1.add(new Entry(Grad,y_data_vz_hs1));

                   if (cyl_in != 0) {
                       s_2 = b_1_in;
                       Sin_e_2 = (s_2-r3)/r3*Math.sin(u_2_rad);
                       Sin_e2 = (1/n_in)*Sin_e_2;
                       e_2 = Math.asin(Sin_e_2);
                       e2 = Math.asin(Sin_e2);
                       u2 = u_2_rad+e_2-e2;
                       s2 = r3*(1+(Sin_e2/Math.sin(u2)));
                       alpha2 = u_2_rad+e_2;

                       s_1 = d_in+s2;
                       u_1 = u2;
                       Sin_e_1 = (s_1-r1)/r1*Math.sin(u_1);
                       e_1 = Math.asin(Sin_e_1);
                       Sin_e1 = (n_in/1)*Sin_e_1;
                       e1 = Math.asin(Sin_e1);
                       alpha1 = u_1+e_1;
                       u1 = u_1+e_1-e1;
                       if (Grad == 0) {
                           u1_absolut2 = u1;
                       }

                       l_t1 = s_1_in*1000;
                       Ast_Konst_1 = (n_in*Math.cos(e_1)-1*Math.cos(e1))/r1;
                       Hilfe1 = 1*Math.cos(e1)*Math.cos(e1)/l_t1;
                       l_t_1 = n_in*Math.cos(e_1)*Math.cos(e_1)/(Ast_Konst_1+Hilfe1);

                       l_s1 = l_t1;
                       Hilfe2 = 1/l_s1;
                       l_s_1 = n_in/(Ast_Konst_1+Hilfe2);

                       p1 = r1*(1-Math.cos(alpha1));
                       p2 = r3*(1-Math.cos(alpha2));
                       ds = (d_in+p2-p1)/Math.cos(u2);

                       l_t2 = l_t_1-ds;
                       Ast_Konst_2 = (1*Math.cos(e_2)-n_in*Math.cos(e2))/r3;
                       Hilfe21 = n_in*Math.cos(e2)*Math.cos(e2)/l_t2;
                       l_t_2 = 1*Math.cos(e_2)*Math.cos(e_2)/(Ast_Konst_2+Hilfe21);

                       l_s2 = l_s_1-ds;
                       Hilfe22 = n_in/l_s2;
                       l_s_2 = 1/(Ast_Konst_2+Hilfe22);

                       delta_s_1 = (r3*(Math.sin(alpha2)/Math.sin(u_2_rad)))-b_1_in;

                       t_sk_1 = l_t_2-delta_s_1;
                       s_sk_1 = l_s_2-delta_s_1;
                       T_sk_1 = 1000/t_sk_1;
                       S_sk_1 = 1000/s_sk_1;
                       mitt_Brechkraft = 0.5*(T_sk_1+S_sk_1);
                       Astigmatismus = T_sk_1-S_sk_1;
                       Anhang = Double.toString(Math.round(Astigmatismus*10000)/10000.0);
                       if (Anhang.length()>10) {
                           Anhang = String.format("%6.4e", Astigmatismus).replace(",",".");
                       }
                       hs2_asti_list = hs2_asti_list + Anhang + " dpt<br>";

                       float y_data_asti_hs2 = Float.parseFloat(String.valueOf(Astigmatismus));
                       yAXES_asti_hs2.add(new Entry(Grad,y_data_asti_hs2));

                       Refraktionsfehler = mitt_Brechkraft-(sph_in+cyl_in)-1000/l_t1;
                       Anhang = Double.toString(Math.round(Refraktionsfehler*10000)/10000.0);
                       if (Anhang.length()>10) {
                           Anhang = String.format("%6.4e", Refraktionsfehler).replace(",",".");
                       }
                       hs2_refra_list = hs2_refra_list + Anhang + " dpt<br>";

                       float y_data_refa_hs2 = Float.parseFloat(String.valueOf(Refraktionsfehler));
                       yAXES_refra_hs2.add(new Entry(Grad,y_data_refa_hs2));

                       Verzeichnung = (Math.tan(u_2_rad)/Math.tan(u1))/
                               (Math.tan(Math.toRadians(0.001))/Math.tan(u1_absolut2))*100-100;
                       Anhang = Double.toString(Math.round(Verzeichnung*100)/100.0);
                       if (Anhang.length()>10) {
                           Anhang = String.format("%6.4e", Verzeichnung).replace(",",".");
                       }
                       hs2_vz_list = hs2_vz_list + Anhang + " %<br>";

                       float y_data_vz_hs2 = Float.parseFloat(String.valueOf(Verzeichnung));
                       yAXES_vz_hs2.add(new Entry(Grad,y_data_vz_hs2));

                   }

                   Grad = Grad + abstufung_in;
                   u_2_rad = Math.toRadians(Grad);
               }

                   ArrayList<ILineDataSet> lineDataSets_refra = new ArrayList<>();
                   ArrayList<ILineDataSet> lineDataSets_asti = new ArrayList<>();
                   ArrayList<ILineDataSet> lineDataSets_vz = new ArrayList<>();

                   LineDataSet lineDataSet_refra_hs1 = new LineDataSet(yAXES_refra_hs1,"HS1");
                   lineDataSet_refra_hs1.setDrawCircles(false);
                   lineDataSet_refra_hs1.setDrawValues(false);
                   lineDataSet_refra_hs1.setColor(Color.rgb(255,127,0));
                   lineDataSet_refra_hs1.setHighlightEnabled(false);
                   lineDataSet_refra_hs1.setLineWidth(1f);

               if (cyl_in != 0) {
                   LineDataSet lineDataSet_refra_hs2 = new LineDataSet(yAXES_refra_hs2, "HS2");
                   lineDataSet_refra_hs2.setDrawCircles(false);
                   lineDataSet_refra_hs2.setDrawValues(false);
                   lineDataSet_refra_hs2.setColor(Color.rgb(30, 144, 255));
                   lineDataSet_refra_hs2.setHighlightEnabled(false);
                   lineDataSet_refra_hs2.setLineWidth(1f);
                   lineDataSets_refra.add(lineDataSet_refra_hs2);
               }

                   lineDataSets_refra.add(lineDataSet_refra_hs1);

                   lc_glasrechner_refra_out.setData(new LineData(lineDataSets_refra));
                   lc_glasrechner_refra_out.setVisibleXRangeMaximum(blickwinkel_in);
                   lc_glasrechner_refra_out.getDescription().setText("Refraktionsfehler");
                   lc_glasrechner_refra_out.getDescription().setTextColor(Color.rgb(255,255,255));
                   lc_glasrechner_refra_out.getDescription().setTextSize(9f);
                   lc_glasrechner_refra_out.getLegend().setTextColor(Color.rgb(255,255,255));

                   LimitLine Zero = new LimitLine(0f);
                   Zero.setLineColor(Color.rgb(255,255,255));
                   Zero.setLineWidth(1f);

                   XAxis xAxis = lc_glasrechner_refra_out.getXAxis();
                   xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                   xAxis.setTextColor(Color.rgb(255,255,255));
                   xAxis.setDrawLabels(true);

                   YAxis yleft = lc_glasrechner_refra_out.getAxisLeft();
                   yleft.setEnabled(false);

                   YAxis yright = lc_glasrechner_refra_out.getAxisRight();
                   yright.setTextColor(Color.rgb(255,255,255));
                   yright.setDrawLabels(true);
                   yright.addLimitLine(Zero);
                   yright.setDrawLimitLinesBehindData(true);

                   lc_glasrechner_refra_out.invalidate();


                   LineDataSet lineDataSet_asti_hs1 = new LineDataSet(yAXES_asti_hs1,"HS1");
                   lineDataSet_asti_hs1.setDrawCircles(false);
                   lineDataSet_asti_hs1.setDrawValues(false);
                   lineDataSet_asti_hs1.setColor(Color.rgb(255,127,0));
                   lineDataSet_asti_hs1.setHighlightEnabled(false);
                   lineDataSet_asti_hs1.setLineWidth(1f);

               if (cyl_in != 0) {
                   LineDataSet lineDataSet_asti_hs2 = new LineDataSet(yAXES_asti_hs2, "HS2");
                   lineDataSet_asti_hs2.setDrawCircles(false);
                   lineDataSet_asti_hs2.setDrawValues(false);
                   lineDataSet_asti_hs2.setColor(Color.rgb(30, 144, 255));
                   lineDataSet_asti_hs2.setHighlightEnabled(false);
                   lineDataSet_asti_hs2.setLineWidth(1f);
                   lineDataSets_asti.add(lineDataSet_asti_hs2);
               }

                   lineDataSets_asti.add(lineDataSet_asti_hs1);


                   lc_glasrechner_asti_out.setData(new LineData(lineDataSets_asti));
                   lc_glasrechner_asti_out.setVisibleXRangeMaximum(blickwinkel_in);
                   lc_glasrechner_asti_out.getDescription().setText("Astigmatismus");
                   lc_glasrechner_asti_out.getDescription().setTextColor(Color.rgb(255,255,255));
                   lc_glasrechner_asti_out.getDescription().setTextSize(9f);
                   lc_glasrechner_asti_out.getLegend().setTextColor(Color.rgb(255,255,255));

                   Zero.setLineColor(Color.rgb(255,255,255));
                   Zero.setLineWidth(1f);

                   xAxis = lc_glasrechner_asti_out.getXAxis();
                   xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                   xAxis.setTextColor(Color.rgb(255,255,255));
                   xAxis.setDrawLabels(true);

                   yleft = lc_glasrechner_asti_out.getAxisLeft();
                   yleft.setEnabled(false);

                   yright = lc_glasrechner_asti_out.getAxisRight();
                   yright.setTextColor(Color.rgb(255,255,255));
                   yright.setDrawLabels(true);
                   yright.addLimitLine(Zero);
                   yright.setDrawLimitLinesBehindData(true);

                   lc_glasrechner_asti_out.invalidate();


                   LineDataSet lineDataSet_vz_hs1 = new LineDataSet(yAXES_vz_hs1,"HS1");
                   lineDataSet_vz_hs1.setDrawCircles(false);
                   lineDataSet_vz_hs1.setDrawValues(false);
                   lineDataSet_vz_hs1.setColor(Color.rgb(255,127,0));
                   lineDataSet_vz_hs1.setHighlightEnabled(false);
                   lineDataSet_vz_hs1.setLineWidth(1f);

               if (cyl_in != 0) {
                   LineDataSet lineDataSet_vz_hs2 = new LineDataSet(yAXES_vz_hs2, "HS2");
                   lineDataSet_vz_hs2.setDrawCircles(false);
                   lineDataSet_vz_hs2.setDrawValues(false);
                   lineDataSet_vz_hs2.setColor(Color.rgb(30, 144, 255));
                   lineDataSet_vz_hs2.setHighlightEnabled(false);
                   lineDataSet_vz_hs2.setLineWidth(1f);
                   lineDataSets_vz.add(lineDataSet_vz_hs2);
               }

                   lineDataSets_vz.add(lineDataSet_vz_hs1);


                   lc_glasrechner_vz_out.setData(new LineData(lineDataSets_vz));
                   lc_glasrechner_vz_out.setVisibleXRangeMaximum(blickwinkel_in);
                   lc_glasrechner_vz_out.getDescription().setText("Verzeichnung");
                   lc_glasrechner_vz_out.getDescription().setTextColor(Color.rgb(255,255,255));
                   lc_glasrechner_vz_out.getDescription().setTextSize(9f);
                   lc_glasrechner_vz_out.getLegend().setTextColor(Color.rgb(255,255,255));

                   Zero.setLineColor(Color.rgb(255,255,255));
                   Zero.setLineWidth(1f);

                   xAxis = lc_glasrechner_vz_out.getXAxis();
                   xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                   xAxis.setTextColor(Color.rgb(255,255,255));
                   xAxis.setDrawLabels(true);

                   yleft = lc_glasrechner_vz_out.getAxisLeft();
                   yleft.setEnabled(false);

                   yright = lc_glasrechner_vz_out.getAxisRight();
                   yright.setTextColor(Color.rgb(255,255,255));
                   yright.setDrawLabels(true);
                   yright.addLimitLine(Zero);
                   yright.setDrawLimitLinesBehindData(true);

                   lc_glasrechner_vz_out.invalidate();


                // Ausgabe der Abbildungsfehler für HS 1

               txt_glasrechner_grad_refra_out.setText(Html.fromHtml(Grad_Anzeige));
               txt_glasrechner_grad_asti_out.setText(Html.fromHtml(Grad_Anzeige));
               txt_glasrechner_grad_vz_out.setText(Html.fromHtml(Grad_Anzeige));

               txt_glasrechner_refra_hs1_out.setText(Html.fromHtml(hs1_refra_list));
               txt_glasrechner_asti_hs1_out.setText(Html.fromHtml(hs1_asti_list));
               txt_glasrechner_vz_hs1_out.setText(Html.fromHtml(hs1_vz_list));


                // Berechnung uns Ausgabe der endgültigen Randdicken und der Bauhöhe

               t1 = r1 * (1 - Math.sqrt(1 - ((Math.pow((durchmesser_in / 2), 2) /
                       (Math.pow(r1, 2))))));
               t2 = r2 * (1 - Math.sqrt(1 - ((Math.pow((durchmesser_in / 2), 2) /
                       (Math.pow(r2, 2))))));

               dr1 = d_in - t1 + t2;

               double V1, V2, VGes, Gewicht;

               V1 = Math.PI * (dr1 * Math.pow((durchmesser_in/2), 2) + Math.pow(t1, 2) *
                       (r1 - (t1/3)) - Math.pow(t2, 2) * (r2 - (t2/3)));

               if (cyl_in != 0) {
                   t3 = r3 * (1 - Math.sqrt(1 - ((Math.pow((durchmesser_in / 2), 2) /
                           (Math.pow(r3, 2))))));


                   dr2 = d_in - t1 + t3;

                   txt_glasrechner_dr3_out.setText(Double.toString(Math.round(dr2*10000)/10000.0));

                   V2 = Math.PI * (dr2 * Math.pow((durchmesser_in/2), 2) + Math.pow(t1, 2) *
                           (r1 - (t1/3)) - Math.pow(t3, 2) * (r3 - (t3/3)));

                   VGes = ((V1 + V2) / 2) / 1000;

                   txt_glasrechner_volumen_out.setText(Double.toString(Math.round(VGes*100)/100.0));

                   Gewicht = VGes * rho_in;

                   txt_glasrechner_gewicht_out.setText(
                           Double.toString(Math.round(Gewicht*100)/100.0));

                   if (dr1 > dr2) {
                       txt_glasrechner_bauhoehe_out.setText(
                               Double.toString(Math.round((t1+dr1)*100)/100.0));
                   } else {
                       txt_glasrechner_bauhoehe_out.setText(
                               Double.toString(Math.round((t1+dr2)*100)/100.0));
                   }
               } else {
                   txt_glasrechner_bauhoehe_out.setText(
                           Double.toString(Math.round((t1+dr1)*100)/100.0));
                   V1 = V1/1000;
                   txt_glasrechner_volumen_out.setText(Double.toString(Math.round(V1*100)/100.0));
                   Gewicht = V1 * rho_in;
                   txt_glasrechner_gewicht_out.setText(
                           Double.toString(Math.round(Gewicht*100)/100.0));
               }

               txt_glasrechner_dr2_out.setText(Double.toString(Math.round(dr1*10000)/10000.0));


               if (cyl_in != 0) {

                    // Ausgabe der Abbildungsfehler für HS 2

                   txt_glasrechner_refra_hs2_out.setText(Html.fromHtml(hs2_refra_list));
                   txt_glasrechner_asti_hs2_out.setText(Html.fromHtml(hs2_asti_list));
                   txt_glasrechner_vz_hs2_out.setText(Html.fromHtml(hs2_vz_list));

                    // Sichtbarkeit für sämtliche Bezeichungs- und Einheitenobjekte für HS 2

                   txt_HS2_wunsch.setVisibility(View.VISIBLE);
                   txt_glasrechner_hs2_out_einheit.setVisibility(View.VISIBLE);
                   txt_F3_real.setVisibility(View.VISIBLE);
                   txt_glasrechner_f3_real_out_einheit.setVisibility(View.VISIBLE);
                   txt_R3.setVisibility(View.VISIBLE);
                   txt_glasrechner_r3_out_einheit.setVisibility(View.VISIBLE);
                   txt_DR3.setVisibility(View.VISIBLE);
                   txt_glasrechner_dr3_out_einheit.setVisibility(View.VISIBLE);
                   txt_glasrechner_hs2_refra.setVisibility(View.VISIBLE);
                   txt_glasrechner_hs2_asti.setVisibility(View.VISIBLE);
                   txt_glasrechner_hs2_vz.setVisibility(View.VISIBLE);

                   txt_glasrechner_hs2_out.setVisibility(View.VISIBLE);
                   txt_glasrechner_f3_real_out.setVisibility(View.VISIBLE);
                   txt_glasrechner_r3_out.setVisibility(View.VISIBLE);
                   txt_glasrechner_dr3_out.setVisibility(View.VISIBLE);
                   txt_glasrechner_refra_hs2_out.setVisibility(View.VISIBLE);
                   txt_glasrechner_asti_hs2_out.setVisibility(View.VISIBLE);
                   txt_glasrechner_vz_hs2_out.setVisibility(View.VISIBLE);
               }
               if (cb_glasrechner_werkzeugindex.isChecked()) {
                   txt_F2_wkn.setVisibility(View.VISIBLE);
                   txt_glasrechner_f2_wkn_out.setVisibility(View.VISIBLE);
                   txt_glasrechner_f2_wkn_out_einheit.setVisibility(View.VISIBLE);
                   if (cyl_in != 0) {
                       txt_F3_wkn.setVisibility(View.VISIBLE);
                       txt_glasrechner_f3_wkn_out.setVisibility(View.VISIBLE);
                       txt_glasrechner_f3_wkn_out_einheit.setVisibility(View.VISIBLE);
                   }
               }
             }
        });
    }
}