package de.HS_Aalen.OptikFormelrechner;

import android.app.ActionBar.LayoutParams;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.InputType;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import static android.util.TypedValue.COMPLEX_UNIT_DIP;


public class Transmission_Berechnung extends AppCompatActivity {

    final Context context = this;
    EditText B1,B2,B3,B4,B5;
    TextView Ausgabe, Ausgabe_zwischenergebnisse, B3_Text;
    CheckBox CB;

    double Reflexion, Transmission, Ergebnis;
    double Transmission_zwischen, Reflexion_zwischen, Ergebnis_zwischen;
    double n_neu;

    int zwischenspeicher;
    String formation = "";

    int Abfrage = 1;
    int cb_check = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.transmission_berechnung);

        // Einlesen der Daten aus der Bundle Funktion aus der vorigen Activity
        Bundle zielkorb = getIntent().getExtras();
        String text2 = zielkorb.getString("datenpaket4");
        final int fn = Integer.parseInt(text2);

        // Verhindert das Öffnen des Tastaturlayouts beim Starten der Activity
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        ScrollView sv = new ScrollView(this);
        LinearLayout ll = new LinearLayout(this);

        ll.setOrientation(LinearLayout.VERTICAL);

        final LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        params.setMargins(200,0,0,0);

        sv.addView(ll);

        Transmission = 1;
        Reflexion = 0;
        Ergebnis =1;

        int k = 1;

        // Erstelle die Eingabefenster und Beschreibungen für die Anzahl "fn" an Flächen
        for (int i = 0; i < fn; i++) {
            TextView tV0 = new TextView(this);
            tV0.setPadding(20,0,0,0);
            tV0.setLines(3);
            tV0.setGravity(Gravity.CENTER_VERTICAL);
            tV0.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
            TextView tV1 = new TextView(this);
            tV1.setPadding(100,0,0,5);
            TextView tV2 = new TextView(this);
            tV2.setPadding(100,0,0,5);
            TextView tV3 = new TextView(this);
            tV3.setPadding(100,0,0,35);
            tV3.setId(k);
            k = k + 1;
            TextView tV4 = new TextView(this);
            tV4.setPadding(100,15,0,25);
            TextView tV5 = new TextView(this);
            tV5.setPadding(100,0,0,15);
            TextView tV6 = new TextView(this);
            tV6.setLines(3);
            tV6.setPadding(20,0,0,0);
            tV6.setGravity(Gravity.CENTER_VERTICAL);
            tV6.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);

            // Füge eine Checkbox hinzu, ob eine Fläche entspiegelt ist
            CheckBox cb_ent = new CheckBox(this);
            cb_ent.setPadding(100,50,0,50);
            cb_ent.setId(k);
            k = k + 1;

            EditText et1 = new EditText(this);
            et1.setLayoutParams(params);
            et1.setPadding(20,0,20,20);
            et1.setEms(4);
            et1.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
            et1.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
            et1.setId(k);
            k = k + 1;
            EditText et2 = new EditText(this);
            et2.setLayoutParams(params);
            et2.setPadding(20,0,20,20);
            et2.setEms(4);
            et2.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
            et2.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
            et2.setId(k);
            k = k + 1;
            EditText et3 = new EditText(this);
            et3.setLayoutParams(params);
            et3.setPadding(20,0,20,20);
            et3.setEms(4);
            et3.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
            et3.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
            et3.setId(k);
            et3.setVisibility(View.GONE);
            k = k + 1;
            EditText et4 = new EditText(this);
            et4.setLayoutParams(params);
            et4.setPadding(20,0,20,20);
            et4.setEms(4);
            et4.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
            et4.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
            et4.setId(k);
            k = k + 1;
            EditText et5 = new EditText(this);
            et5.setLayoutParams(params);
            et5.setPadding(20,0,20,20);
            et5.setEms(4);
            et5.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
            et5.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
            et5.setId(k);
            k = k + 1;

            tV0.setText("Fläche: " + (i + 1));
            // Der Brechungindex n1 vor der ersten Fläche wird nur ein einziges Mal benötigt
            if (i == 0) {
                tV1.setText(Html.fromHtml("n" + "<sub><small>" + (i + 1) + "</small></sub>"));
            }
            tV2.setText(Html.fromHtml("n" + "<sub><small>" + (i + 2) + "</small></sub>"));
            cb_ent.setText("Fläche " + (i + 1) + " ist entspiegelt");
            tV3.setText(Html.fromHtml("Restreflex р" + "<sub><small>" + (i + 1) +
                    "</small></sub> [%]"));
            tV3.setVisibility(View.GONE);
            tV4.setText(Html.fromHtml("Τ<sub><small>0" + "<sub><small>" + (i + 1) +
                    "</small></sub></small></sub> [%]"));
            tV5.setText(Html.fromHtml("d" + "<sub><small>" + (i + 1) + "</small></sub> [mm]"));
            tV6.setText("Abstand zwischen Fläche " + (i+1) + " & " + (i+2));


            ll.addView(tV0);
            if (i == 0) {

                ll.addView(tV1);
                ll.addView(et1);
            }
            ll.addView(tV2);
            ll.addView(et2);
            ll.addView(cb_ent);
            ll.addView(tV3);
            ll.addView(et3);
            if (i < fn - 1) {
                ll.addView(tV4);
                ll.addView(et4);
                ll.addView(tV6);
                ll.addView(tV5);
                ll.addView(et5);
            }
        }

        Button bt = new Button(this);
        bt.setText("Berechnen");
        bt.setAllCaps(false);
        ll.addView(bt);

        // Erstellen der TextView für das Endergebniss
        final TextView tv = new TextView(this);
        tv.setText("");
        tv.setId(0);
        tv.setVisibility(View.GONE);
        tv.setWidth(200);
        tv.setTextSize(COMPLEX_UNIT_DIP, 20);
        tv.setPadding(100,50,0,50);
        ll.addView(tv);

            // Erstellen der TextView für die Zwischenergebnisse
            final TextView zwischenergebnisse = new TextView(this);
            zwischenspeicher = fn*7+1;
            zwischenergebnisse.setId(zwischenspeicher);

            zwischenergebnisse.setPadding(15,0,0,15);
            zwischenergebnisse.setVisibility(View.GONE);
            zwischenergebnisse.setTextSize(COMPLEX_UNIT_DIP, 17);
            ll.addView(zwischenergebnisse);


        this.setContentView(sv);



        bt.setOnClickListener (new View.OnClickListener() {
            public void onClick(View v) {

                int k = 1;
                //Überprüfe für alle erstellten Checkboxen, ob der Haken für eine entspiegelte
                // Fläche gesetzt wurde und füge gegebenfalls das entsprechende Eingabefenster
                // hinzu um den prozentualen Restreflex zu setzen
                if (cb_check == 0) {
                    for (int y = 1; y <= fn; y++) {

                        CB = (CheckBox) findViewById(k + 1);
                        if (CB.isChecked()) {
                            B3 = (EditText) findViewById(k + 4);
                            B3.setVisibility(View.VISIBLE);
                            B3_Text = (TextView) findViewById(k);
                            B3_Text.setVisibility(View.VISIBLE);
                            cb_check = 1;
                        } else {
                            CB.setEnabled(false);
                            B3 = (EditText) findViewById(k+4);
                            B3.setText("1");
                        }
                        k = k + 7;
                    }
                }

                k = 1;

                    // Sollte der Haken bei einer Checkbox gesetzt worden sein, dann weise den
                // Nutzer darauf hin, dass er nun den prozentualen Restreflex eingeben kann
                    if (cb_check == 1) {
                        AlertDialog.Builder AlarmBox = new AlertDialog.Builder(context);
                        AlarmBox.setTitle("Layout wurde erweitert!");
                        AlarmBox.setMessage(Html.fromHtml("Für die entspiegelten Flächen " +
                                "den Restreflex angeben"));
                        AlarmBox.setNeutralButton("OK", null);

                        AlertDialog dialog = AlarmBox.create();
                        dialog.show();
                        cb_check = 2;
                        return;

                    } else if (cb_check == 0) {

                        if (fn > 1) {
                            zwischenergebnisse.setVisibility(View.VISIBLE);
                            if (fn == 2) {
                                formation = "Zwischenergebnis: <br>";
                            } else {
                                formation = "Zwischenergebnisse: <br>";
                            }
                        }

                        // Berechnung der Transmission
                        for (int i = 1; i <= fn; i++) {
                            if (i == 1) {

                                // Abfrage ob Felder alle einen Wert haben
                                B1 = (EditText) findViewById(k+2);
                                String Alarm = B1.getText().toString();
                                if (Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") ||
                                        Alarm.equals("+")) {
                                    AlertDialog.Builder AlarmBox = new AlertDialog.Builder(context);
                                    AlarmBox.setTitle("Leeres Feld!");
                                    AlarmBox.setMessage(Html.fromHtml("n<sub><small>1</small>" +
                                            "</sub> ist leer<br>"));
                                    AlarmBox.setNeutralButton("OK", null);

                                    AlertDialog dialog = AlarmBox.create();
                                    dialog.show();
                                    tv.setVisibility(View.GONE);
                                    zwischenergebnisse.setVisibility(View.GONE);
                                    return;
                                }
                                double N1 = Double.parseDouble(B1.getText().toString());

                                B2 = (EditText) findViewById(k+3);
                                Alarm = B2.getText().toString();
                                if (Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") ||
                                        Alarm.equals("+")) {
                                    AlertDialog.Builder AlarmBox = new AlertDialog.Builder(context);
                                    AlarmBox.setTitle("Leeres Feld!");
                                    AlarmBox.setMessage(Html.fromHtml("n<sub><small>2</small>" +
                                            "</sub> ist leer<br>"));
                                    AlarmBox.setNeutralButton("OK", null);

                                    AlertDialog dialog = AlarmBox.create();
                                    dialog.show();
                                    tv.setVisibility(View.GONE);
                                    zwischenergebnisse.setVisibility(View.GONE);
                                    return;
                                }
                                double N2 = Double.parseDouble(B2.getText().toString());

                                if (i < fn) {
                                    B4 = (EditText) findViewById(k+5);
                                    Alarm = B4.getText().toString();
                                    if (Alarm.equals("") || Alarm.equals(".") ||
                                            Alarm.equals("-") || Alarm.equals("+")) {
                                        AlertDialog.Builder AlarmBox =
                                                new AlertDialog.Builder(context);
                                        AlarmBox.setTitle("Leeres Feld!");
                                        AlarmBox.setMessage(Html.fromHtml("Τ<sub><small>0<sub>" +
                                                "<small>1</small></sub></small>" +
                                                "</sub> ist leer<br>"));
                                        AlarmBox.setNeutralButton("OK", null);

                                        AlertDialog dialog = AlarmBox.create();
                                        dialog.show();
                                        tv.setVisibility(View.GONE);
                                        zwischenergebnisse.setVisibility(View.GONE);
                                        return;
                                    }
                                    double T0 = Double.parseDouble(B4.getText().toString());

                                    B5 = (EditText) findViewById(k+6);
                                    Alarm = B5.getText().toString();
                                    if (Alarm.equals("") || Alarm.equals(".") ||
                                            Alarm.equals("-") || Alarm.equals("+")) {
                                        AlertDialog.Builder AlarmBox =
                                                new AlertDialog.Builder(context);
                                        AlarmBox.setTitle("Leeres Feld!");
                                        AlarmBox.setMessage(Html.fromHtml("d<sub><small>1</small>" +
                                                "</sub> ist leer<br>"));
                                        AlarmBox.setNeutralButton("OK", null);

                                        AlertDialog dialog = AlarmBox.create();
                                        dialog.show();
                                        tv.setVisibility(View.GONE);
                                        zwischenergebnisse.setVisibility(View.GONE);
                                        return;
                                    }
                                    double d = Double.parseDouble(B5.getText().toString());

                                    T0 = T0/100;
                                    Transmission = Math.pow(T0, (d/10));
                                    Transmission_zwischen = Transmission*100;
                                }

                                k = k + 7;

                                N1 = N1/100;
                                N2 = N2/100;

                                Reflexion = Math.pow(((N1-N2)/(N1+N2)),2);
                                Reflexion_zwischen = Reflexion*100;
                                Reflexion = 1-Reflexion;

                                n_neu = N2;

                                Ergebnis = Transmission*Reflexion;
                                Ergebnis_zwischen = Ergebnis*100;

                                if (i < fn) {
                                    formation = formation + "p" + "<sub><small>1</small></sub>" +
                                            " = " + Double.toString(
                                                    Math.round(Reflexion_zwischen*10000)/10000.0) +
                                            " %<br>Τ<sub><small>1</small></sub> = " +
                                            Double.toString(Math.round(
                                                    Transmission_zwischen*10000)/10000.0) +
                                            " %<br>Τ<sub><small>1<sub><small>Ges</small>" +
                                            "</sub></small></sub> = " + Double.toString(Math.round(
                                                    Ergebnis_zwischen*10000)/10000.0) +
                                            " %<br><br>";
                                    Ausgabe_zwischenergebnisse = (TextView)
                                            findViewById(zwischenspeicher);
                                    Ausgabe_zwischenergebnisse.setText(Html.fromHtml(formation));
                                }

                            } else {

                                B2 = (EditText) findViewById(k+3);
                                String Alarm = B2.getText().toString();
                                if (Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") ||
                                        Alarm.equals("+")) {
                                    AlertDialog.Builder AlarmBox = new AlertDialog.Builder(context);
                                    AlarmBox.setTitle("Leeres Feld!");
                                    AlarmBox.setMessage(Html.fromHtml("n<sub><small>"+(i+1)+
                                            "</small></sub> ist leer<br>"));
                                    AlarmBox.setNeutralButton("OK", null);

                                    AlertDialog dialog = AlarmBox.create();
                                    dialog.show();
                                    tv.setVisibility(View.GONE);
                                    zwischenergebnisse.setVisibility(View.GONE);
                                    return;
                                }
                                double N2 = Double.parseDouble(B2.getText().toString());

                                if (i < fn) {
                                    B4 = (EditText) findViewById(k+5);
                                    Alarm = B4.getText().toString();
                                    if (Alarm.equals("") || Alarm.equals(".") ||
                                            Alarm.equals("-") || Alarm.equals("+")) {
                                        AlertDialog.Builder AlarmBox =
                                                new AlertDialog.Builder(context);
                                        AlarmBox.setTitle("Leeres Feld!");
                                        AlarmBox.setMessage(Html.fromHtml(
                                                "Τ<sub><small>0<sub><small>" + i +
                                                        "</small></sub></small>" +
                                                        "</sub> ist leer<br>"));
                                        AlarmBox.setNeutralButton("OK", null);

                                        AlertDialog dialog = AlarmBox.create();
                                        dialog.show();
                                        tv.setVisibility(View.GONE);
                                        zwischenergebnisse.setVisibility(View.GONE);
                                        return;
                                    }
                                    double T0 = Double.parseDouble(B4.getText().toString());

                                    B5 = (EditText) findViewById(k+6);
                                    Alarm = B5.getText().toString();
                                    if (Alarm.equals("") || Alarm.equals(".") ||
                                            Alarm.equals("-") || Alarm.equals("+")) {
                                        AlertDialog.Builder AlarmBox =
                                                new AlertDialog.Builder(context);
                                        AlarmBox.setTitle("Leeres Feld!");
                                        AlarmBox.setMessage(Html.fromHtml("d<sub><small>"+i+
                                                "</small> ist leer<br>"));
                                        AlarmBox.setNeutralButton("OK", null);

                                        AlertDialog dialog = AlarmBox.create();
                                        dialog.show();
                                        tv.setVisibility(View.GONE);
                                        zwischenergebnisse.setVisibility(View.GONE);
                                        return;
                                    }
                                    double d = Double.parseDouble(B5.getText().toString());

                                    T0 = T0/100;
                                    Transmission = Math.pow(T0, (d/10));
                                    Transmission_zwischen = Transmission*100;
                                }

                                N2 = N2/100;
                                Reflexion = Math.pow(((n_neu-N2)/(n_neu+N2)),2);
                                Reflexion_zwischen = Reflexion*100;
                                Reflexion = 1-Reflexion;
                                n_neu = N2;

                                if (i < fn) {
                                    Ergebnis = Ergebnis * Transmission * Reflexion;

                                // Nach der letzten Fläche wird keine Transmission benötigt
                                } else {
                                    Ergebnis = Ergebnis * Reflexion;
                                }
                                Ergebnis_zwischen = Ergebnis*100;

                                k = k + 7;

                                // So lange die letzte Fläche nicht erreicht wurde,
                                // schreibe die Ergebnisse als Zwischenergebnisse
                                if (i < fn) {
                                    formation = formation + "p" + "<sub><small>"+i+
                                            "</small></sub>" +" = " +
                                            Double.toString(Math.round(
                                                    Reflexion_zwischen*10000)/10000.0) +
                                            " %<br>Τ<sub><small>"+i+"</small></sub> = " +
                                            Double.toString(Math.round(
                                                    Transmission_zwischen*10000)/10000.0) +
                                            " %<br>Τ<sub><small>"+i+"<sub><small>Ges</small>" +
                                            "</sub></small></sub> = " +
                                            Double.toString(Math.round(
                                                    Ergebnis_zwischen*10000)/10000.0) +
                                            " %<br><br>";
                                    Ausgabe_zwischenergebnisse = (TextView)
                                            findViewById(zwischenspeicher);
                                    Ausgabe_zwischenergebnisse.setText(Html.fromHtml(formation));
                                }
                            }
                        }
                    } else if (cb_check == 2) {

                        if (fn > 1) {
                            zwischenergebnisse.setVisibility(View.VISIBLE);
                            if (fn == 2) {
                                formation = "Zwischenergebnis: <br>";
                            } else {
                                formation = "Zwischenergebnisse: <br>";
                            }
                        }

                        for (int i = 1; i <= fn; i++) {

                            if (i == 1) {

                                B1 = (EditText) findViewById(k + 2);
                                String Alarm = B1.getText().toString();
                                if (Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") ||
                                        Alarm.equals("+")) {
                                    AlertDialog.Builder AlarmBox = new AlertDialog.Builder(context);
                                    AlarmBox.setTitle("Leeres Feld!");
                                    AlarmBox.setMessage(Html.fromHtml("n<sub><small>1</small>" +
                                            "</sub> ist leer<br>"));
                                    AlarmBox.setNeutralButton("OK", null);

                                    AlertDialog dialog = AlarmBox.create();
                                    dialog.show();
                                    tv.setVisibility(View.GONE);
                                    zwischenergebnisse.setVisibility(View.GONE);
                                    return;
                                }
                                double N1 = Double.parseDouble(B1.getText().toString());

                                B2 = (EditText) findViewById(k + 3);
                                Alarm = B2.getText().toString();
                                if (Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") ||
                                        Alarm.equals("+")) {
                                    AlertDialog.Builder AlarmBox = new AlertDialog.Builder(context);
                                    AlarmBox.setTitle("Leeres Feld!");
                                    AlarmBox.setMessage(Html.fromHtml("n<sub><small>2</small>" +
                                            "</sub> ist leer<br>"));
                                    AlarmBox.setNeutralButton("OK", null);

                                    AlertDialog dialog = AlarmBox.create();
                                    dialog.show();
                                    tv.setVisibility(View.GONE);
                                    zwischenergebnisse.setVisibility(View.GONE);
                                    return;
                                }
                                double N2 = Double.parseDouble(B2.getText().toString());

                                B3 = (EditText) findViewById(k + 4);
                                Alarm = B3.getText().toString();
                                if (Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") ||
                                        Alarm.equals("+")) {
                                    AlertDialog.Builder AlarmBox = new AlertDialog.Builder(context);
                                    AlarmBox.setTitle("Leeres Feld!");
                                    AlarmBox.setMessage(Html.fromHtml("p<sub><small>" + i +
                                            "</small></sub> ist leer<br>"));
                                    AlarmBox.setNeutralButton("OK", null);

                                    AlertDialog dialog = AlarmBox.create();
                                    dialog.show();
                                    tv.setVisibility(View.GONE);
                                    zwischenergebnisse.setVisibility(View.GONE);
                                    return;
                                }

                                double R_flx = Double.parseDouble(B3.getText().toString());

                                if (i < fn) {
                                    B4 = (EditText) findViewById(k + 5);
                                    Alarm = B4.getText().toString();
                                    if (Alarm.equals("") || Alarm.equals(".") ||
                                            Alarm.equals("-") || Alarm.equals("+")) {
                                        AlertDialog.Builder AlarmBox =
                                                new AlertDialog.Builder(context);
                                        AlarmBox.setTitle("Leeres Feld!");
                                        AlarmBox.setMessage(Html.fromHtml("Τ<sub><small>0<sub>" +
                                                "<small>1</small></sub></small>" +
                                                "</sub> ist leer<br>"));
                                        AlarmBox.setNeutralButton("OK", null);

                                        AlertDialog dialog = AlarmBox.create();
                                        dialog.show();
                                        tv.setVisibility(View.GONE);
                                        zwischenergebnisse.setVisibility(View.GONE);
                                        return;
                                    }
                                    double T0 = Double.parseDouble(B4.getText().toString());

                                    B5 = (EditText) findViewById(k + 6);
                                    Alarm = B5.getText().toString();
                                    if (Alarm.equals("") || Alarm.equals(".") ||
                                            Alarm.equals("-") || Alarm.equals("+")) {
                                        AlertDialog.Builder AlarmBox =
                                                new AlertDialog.Builder(context);
                                        AlarmBox.setTitle("Leeres Feld!");
                                        AlarmBox.setMessage(Html.fromHtml("d<sub><small>1</small>" +
                                                "</sub> ist leer<br>"));
                                        AlarmBox.setNeutralButton("OK", null);

                                        AlertDialog dialog = AlarmBox.create();
                                        dialog.show();
                                        tv.setVisibility(View.GONE);
                                        zwischenergebnisse.setVisibility(View.GONE);
                                        return;
                                    }
                                    double d = Double.parseDouble(B5.getText().toString());

                                    T0 = T0/100;
                                    Transmission = Math.pow(T0, (d/10));
                                    Transmission_zwischen = Transmission*100;
                                }

                                k = k + 7;


                                N1 = N1/100;
                                N2 = N2/100;

                                CB = (CheckBox) findViewById(k - 6);
                                if (CB.isChecked()) {
                                    Reflexion = 1-(R_flx/100);
                                    Reflexion_zwischen = R_flx;
                                } else {
                                    Reflexion = Math.pow(((N1 - N2) / (N1 + N2)), 2);
                                    Reflexion_zwischen = Reflexion * 100;
                                    Reflexion = 1 - Reflexion;
                                }

                                n_neu = N2;

                                Ergebnis = Transmission*Reflexion;
                                Ergebnis_zwischen = Ergebnis*100;

                                if (i < fn) {
                                    formation = formation + "p" + "<sub><small>1</small></sub>" +
                                            " = " + Double.toString(Math.round(
                                                    Reflexion_zwischen*10000)/10000.0) +
                                            " %<br>Τ<sub><small>1</small></sub> = " +
                                            Double.toString(Math.round(
                                                    Transmission_zwischen*10000)/10000.0) +
                                            " %<br>Τ<sub><small>1<sub><small>Ges</small>" +
                                            "</sub></small></sub> = " +
                                            Double.toString(Math.round(
                                                    Ergebnis_zwischen*10000)/10000.0) +
                                            " %<br><br>";
                                    Ausgabe_zwischenergebnisse = (TextView)
                                            findViewById(zwischenspeicher);
                                    Ausgabe_zwischenergebnisse.setText(Html.fromHtml(formation));
                                }

                            } else {

                                B2 = (EditText) findViewById(k + 3);
                                String Alarm = B2.getText().toString();
                                if (Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") ||
                                        Alarm.equals("+")) {
                                    AlertDialog.Builder AlarmBox = new AlertDialog.Builder(context);
                                    AlarmBox.setTitle("Leeres Feld!");
                                    AlarmBox.setMessage(Html.fromHtml("n<sub><small>" +
                                            (i + 1) + "</small></sub> ist leer<br>"));
                                    AlarmBox.setNeutralButton("OK", null);

                                    AlertDialog dialog = AlarmBox.create();
                                    dialog.show();
                                    tv.setVisibility(View.GONE);
                                    zwischenergebnisse.setVisibility(View.GONE);
                                    return;
                                }
                                double N2 = Double.parseDouble(B2.getText().toString());

                                B3 = (EditText) findViewById(k + 4);
                                Alarm = B3.getText().toString();
                                if (Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") ||
                                        Alarm.equals("+")) {
                                    AlertDialog.Builder AlarmBox = new AlertDialog.Builder(context);
                                    AlarmBox.setTitle("Leeres Feld!");
                                    AlarmBox.setMessage(Html.fromHtml("p<sub><small>" + i +
                                            "</small></sub> ist leer<br>"));
                                    AlarmBox.setNeutralButton("OK", null);

                                    AlertDialog dialog = AlarmBox.create();
                                    dialog.show();
                                    tv.setVisibility(View.GONE);
                                    zwischenergebnisse.setVisibility(View.GONE);
                                    return;
                                }
                                double R_flx = Double.parseDouble(B3.getText().toString());

                                if (i < fn) {
                                    B4 = (EditText) findViewById(k + 5);
                                    Alarm = B4.getText().toString();
                                    if (Alarm.equals("") || Alarm.equals(".") ||
                                            Alarm.equals("-") || Alarm.equals("+")) {
                                        AlertDialog.Builder AlarmBox =
                                                new AlertDialog.Builder(context);
                                        AlarmBox.setTitle("Leeres Feld!");
                                        AlarmBox.setMessage(Html.fromHtml("Τ<sub><small>0<sub>" +
                                                "<small>" + i + "</small></sub></small>" +
                                                "</sub> ist leer<br>"));
                                        AlarmBox.setNeutralButton("OK", null);

                                        AlertDialog dialog = AlarmBox.create();
                                        dialog.show();
                                        tv.setVisibility(View.GONE);
                                        zwischenergebnisse.setVisibility(View.GONE);
                                        return;
                                    }
                                    double T0 = Double.parseDouble(B4.getText().toString());

                                    B5 = (EditText) findViewById(k + 6);
                                    Alarm = B5.getText().toString();
                                    if (Alarm.equals("") || Alarm.equals(".") ||
                                            Alarm.equals("-") || Alarm.equals("+")) {
                                        AlertDialog.Builder AlarmBox =
                                                new AlertDialog.Builder(context);
                                        AlarmBox.setTitle("Leeres Feld!");
                                        AlarmBox.setMessage(Html.fromHtml("d<sub><small>" + i +
                                                "</small> ist leer<br>"));
                                        AlarmBox.setNeutralButton("OK", null);

                                        AlertDialog dialog = AlarmBox.create();
                                        dialog.show();
                                        tv.setVisibility(View.GONE);
                                        zwischenergebnisse.setVisibility(View.GONE);
                                        return;
                                    }
                                    double d = Double.parseDouble(B5.getText().toString());

                                    T0 = T0/100;
                                    Transmission = Math.pow(T0, (d/10));
                                    Transmission_zwischen = Transmission*100;
                                }

                                k = k + 7;

                                N2 = N2/100;

                                CB = (CheckBox) findViewById(k - 6);
                                if (CB.isChecked()) {
                                    Reflexion = 1-(R_flx/100);
                                    Reflexion_zwischen = R_flx;
                                } else {
                                    Reflexion = Math.pow(((n_neu - N2) / (n_neu + N2)), 2);
                                    Reflexion_zwischen = Reflexion * 100;
                                    Reflexion = 1 - Reflexion;
                                }
                                n_neu = N2;

                                if (i < fn) {
                                    Ergebnis = Ergebnis * Transmission * Reflexion;
                                } else {
                                    Ergebnis = Ergebnis * Reflexion;
                                }
                                Ergebnis_zwischen = Ergebnis*100;

                                if (i < fn) {
                                    formation = formation + "p" + "<sub><small>"+i+
                                            "</small></sub>" +" = " +
                                            Double.toString(Math.round(
                                                    Reflexion_zwischen*10000)/10000.0) +
                                            " %<br>Τ<sub><small>"+i+"</small></sub> = " +
                                            Double.toString(Math.round(
                                                    Transmission_zwischen*10000)/10000.0) +
                                            " %<br>Τ<sub><small>"+i+"<sub><small>Ges</small>" +
                                            "</sub></small></sub> = " +
                                            Double.toString(Math.round(
                                                    Ergebnis_zwischen*10000)/10000.0) +
                                            " %<br><br>";
                                    Ausgabe_zwischenergebnisse = (TextView)
                                            findViewById(zwischenspeicher);
                                    Ausgabe_zwischenergebnisse.setText(Html.fromHtml(formation));
                                }
                            }
                        }
                    }



                tv.setVisibility(View.VISIBLE);

                    Ausgabe = (TextView) findViewById(0);

                    if (fn == 1) {
                        Ausgabe.setText(Html.fromHtml("Τ<sub><small>"+fn+
                                "<sub><small>Ges</small></sub></small></sub> = " +
                                Double.toString(Math.round(Ergebnis_zwischen*10000)/10000.0) +
                                " %<br><br>p" + "<sub><small>"+fn+"</small></sub>" +" = " +
                                Double.toString(Math.round(Reflexion_zwischen*10000)/10000.0) +
                                " %<br>"));
                    } else {
                        Ausgabe.setText(Html.fromHtml("Flächen: " + fn +
                                "<br>Τ<sub><small>"+fn+
                                "<sub><small>Ges</small></sub></small></sub> = " +
                                Double.toString(Math.round(Ergebnis_zwischen*10000)/10000.0) +
                                " %<br><br>p" + "<sub><small>"+fn+"</small></sub>" +" = " +
                                Double.toString(Math.round(Reflexion_zwischen*10000)/10000.0) +
                                " %<br>"));
                    }
                }

        });
    }

        }