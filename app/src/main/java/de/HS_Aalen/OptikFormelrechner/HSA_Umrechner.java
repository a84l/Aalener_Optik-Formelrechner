package de.HS_Aalen.OptikFormelrechner;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

public class HSA_Umrechner extends AppCompatActivity {

    EditText Sph_in, Cyl_in, HSA_in, HSA_out;
    TextView Sph_out, Cyl_out;
    SeekBar HSA_bar_in, HSA_bar_out;
    Button Berechnen, Clear, Runden;
    final Context context = this;

    int Runden_counter, prevent_loop;
    double raw_sph_out, raw_cyl_out;
    double round_sph_out, round_cyl_out;

    // Erstellt einen Filter der die Eingabe des HSA auf  max 40 mm beschränkt
    public class InputFilterMinMax implements InputFilter {
        private int min;
        private int max;

        public InputFilterMinMax(int min, int max) {
            this.min = min;
            this.max = max;
        }

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest,
                                   int dstart, int dend) {

            try {
                double input = Double.parseDouble(dest.subSequence(0, dstart).toString() +
                        source + dest.subSequence(dend, dest.length()));
                if (isInRange(min, max, input))
                    return null;
            } catch (NumberFormatException nfe) { }
            return "";
        }

        private boolean isInRange(double a, double b, double c) {
            return b > a ? c >= a && c <= b : c >= b && c <= a;
        }
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hsa_umrechner);

        // Verhindert das Öffnen des Tastaturlayouts beim Starten der Activity
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        Sph_in = (EditText) findViewById(R.id.in_sph);
        Cyl_in = (EditText) findViewById(R.id.in_cyl);
        HSA_in = (EditText) findViewById(R.id.in_hsa);
        HSA_in.setFilters(new InputFilter[]{new InputFilterMinMax(0, 40)});
        HSA_out = (EditText) findViewById(R.id.out_hsa);
        HSA_out.setFilters(new InputFilter[]{new InputFilterMinMax(0, 40)});

        Sph_out = (TextView) findViewById(R.id.out_sph);
        Cyl_out = (TextView) findViewById(R.id.out_cyl);

        HSA_bar_in = (SeekBar) findViewById(R.id.hsa_seekbar_in);
        HSA_bar_out = (SeekBar) findViewById(R.id.hsa_seekbar_out);

        Berechnen = (Button) findViewById(R.id.button);
        Clear = (Button) findViewById(R.id.bt_clear);
        Runden = (Button) findViewById(R.id.bt_runden);

        Runden_counter = 0;
        prevent_loop = 0;

        // Liest den Status aus dem EditText HSA_in aus und weißt den Betrag
        // der Seekbar HSA_bar_in zu
        HSA_in.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                prevent_loop = 1;

                try {
                    HSA_bar_in.setProgress(Integer.parseInt(editable.toString()));
                } catch (Exception ex) {}

            }
        });

        // Liest den Status aus dem EditText HSA_out aus und weißt den Betrag
        // der Seekbar HSA_bar_out zu
        HSA_out.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                prevent_loop = 1;

                try {
                    HSA_bar_out.setProgress(Integer.parseInt(editable.toString()));
                } catch (Exception ex) {}

            }
        });

        // Liest den Status aus der Seekbar HSA_bar_in aus und weißt den Betrag
        // dem EditText HSA_in zu
        HSA_bar_in.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) {

                if (prevent_loop == 0) {
                    HSA_in.setText(Integer.toString(progress));

                }

                HSA_berechnen();
                prevent_loop = 0;
            }
        });

        // Liest den Status aus der Seekbar HSA_bar_out aus und weißt den Betrag
        // dem EditText HSA_out zu
        HSA_bar_out.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) {

                if (prevent_loop == 0) {
                    HSA_out.setText(Integer.toString(progress));

                }

                HSA_berechnen();
                prevent_loop = 0;
            }
        });

        Berechnen.setOnClickListener(
                new Button.OnClickListener(){
                    public void onClick(View v) {
                        HSA_berechnen();
                    }
                }
        );

        Clear.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        Sph_in.setText("");
                        Sph_out.setText("");

                        Cyl_in.setText("");
                        Cyl_out.setText("");

                        HSA_in.setText("16");
                        HSA_out.setText("0");

                        Runden.setText("Runden (0.12)");
                        Runden_counter = 0;

                        raw_sph_out = 0;
                        raw_cyl_out = 0;

                        round_sph_out = 0;
                        round_cyl_out = 0;
                    }
                }
        );

        Runden.setOnClickListener(
                new Button.OnClickListener()  {
                    public void onClick(View v) {
                        if (Runden_counter == 0) {
                            String Alarm = Sph_out.getText().toString();
                            String Alarm2 = Sph_out.getText().toString();
                            if (Alarm.equals("") && Alarm2.equals("")) {
                               return;
                            } else {

                            Runden.setText("Runden (0.25)");
                            Runden_counter = 1;

                            raw_sph_out = Double.parseDouble(Sph_out.getText().toString());
                            raw_cyl_out = Double.parseDouble(Cyl_out.getText().toString());

                            round_sph_out = Math.round(raw_sph_out*8)/8f;
                            round_cyl_out = Math.round(raw_cyl_out*8)/8f;

                            Sph_out.setText(Double.toString(round_sph_out));
                            Cyl_out.setText(Double.toString(round_cyl_out));
                        }
                        } else {

                            if (Runden_counter == 1) {
                                Runden.setText("Original");
                                Runden_counter = 2;

                                round_sph_out = Math.round(raw_sph_out*4)/4f;
                                round_cyl_out = Math.round(raw_cyl_out*4)/4f;

                                Sph_out.setText(Double.toString(round_sph_out));
                                Cyl_out.setText(Double.toString(round_cyl_out));
                            } else {

                                if (Runden_counter == 2) {
                                    Runden.setText("Runden (0.12)");
                                    Runden_counter = 0;

                                    Sph_out.setText(Double.toString(raw_sph_out));
                                    Cyl_out.setText(Double.toString(raw_cyl_out));

                                    raw_sph_out = 0;
                                    raw_cyl_out = 0;

                                    round_sph_out = 0;
                                    round_cyl_out = 0;
                                }
                            }
                        }

                    }
                }
        );


    }


    private void HSA_berechnen() {
        String Alarm;
        Alarm = Sph_in.getText().toString();
        if (Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") ||
                Alarm.equals("+")) {
            Sph_in.setText("0");
        }
        double b_sph_in = Double.parseDouble(Sph_in.getText().toString());
        Alarm = Cyl_in.getText().toString();
        if (Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") ||
                Alarm.equals("+")) {
            Cyl_in.setText("0");
        }
        double b_cyl_in = Double.parseDouble(Cyl_in.getText().toString());
        Alarm = HSA_in.getText().toString();
        if (Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") ||
                Alarm.equals("+")) {
            HSA_in.setText("0");
        }
        double b_hsa_in = Double.parseDouble(HSA_in.getText().toString());
        Alarm = HSA_out.getText().toString();
        if (Alarm.equals("") || Alarm.equals(".") || Alarm.equals("-") ||
                Alarm.equals("+")) {
            HSA_out.setText("0");
        }
        double b_hsa_out = Double.parseDouble(HSA_out.getText().toString());

        double HS1 = b_sph_in;
        double HS2 = b_sph_in + b_cyl_in;

        b_hsa_in = b_hsa_in / 1000;
        b_hsa_out = b_hsa_out / 1000;

        double b_HS1 = HS1 / (1 + (b_hsa_out - b_hsa_in) * HS1);
        double b_HS2 = HS2 / (1 + (b_hsa_out - b_hsa_in) * HS2);

        double b_sph_out = b_HS1;
        double b_cyl_out = -(b_HS1 - b_HS2);

        b_sph_out = Math.round(b_sph_out * 100.0) / 100.0;
        b_cyl_out = Math.round(b_cyl_out * 100.0) / 100.0;

        Sph_out.setText(Double.toString(b_sph_out));
        String Ausgabe = Sph_out.getText().toString();
        if (Ausgabe.length() > 9) {
            Sph_out.setText(String.format("%9.4e", b_sph_out).replace(",","."));
        }

        Cyl_out.setText(Double.toString(b_cyl_out));
        Ausgabe = Cyl_out.getText().toString();
        if (Ausgabe.length() > 9) {
            Sph_out.setText(String.format("%9.4e", b_cyl_out).replace(",","."));
        }

        Runden.setText("Runden (0.12)");
        Runden_counter = 0;
    }

}
