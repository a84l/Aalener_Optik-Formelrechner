package de.HS_Aalen.OptikFormelrechner;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class Info extends AppCompatActivity {

    ImageView banner_hs;
    TextView datenschutz, datenverwendung, aenderungen;
    TextView absturz, berechtigungen, webseiten, source, lizenz;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info);

        banner_hs = (ImageView) findViewById(R.id.banner_hs);

        // Zeige das Bild des Hochschullogos "banner_hs" nur in Porträt- und nicht im Landscapemodus
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            banner_hs.setVisibility(View.GONE);
        }
        else {
            banner_hs.setVisibility(View.VISIBLE);
        }

        String datenschutz_textinhalt = "<big><b>Datenschutzerklärung</b></big><br><br><small><small>Stand: 19.08.2018<br><br>" +
                "Entwickler: B. Sc. René Nierath<br>" +
                "Versionsnummer: 1.10<br>" +
                "<br>" +
                "Studiengang Augenoptik/Optometrie der Hochschule Aalen<br>" +
                "Studiendekan: Prof. Dr. Jürgen Nolting<br>" +
                "Anton-Huber-Str. 23<br>" +
                "73430 Aalen<br>" +
                "<br>" +
                "<b>Dem Entwickler und der Hochschule Aalen ist der Schutz Ihrer personenbezogenen Daten sehr wichtig.</b><br>" +
                "<br>" +
                "Durch die Nutzung der App werden keine geräte- oder personenbezogenen Daten erhoben. Sollten Sie im Falle eines Problems einen Absturzbericht über das Formular von <b>Google Play</b> absenden, so machen Sie sich bitte mit den Datenschutzbestimmungen des Service von <b>Google Inc.</b> vertraut.<br>" +
                "<br>" +
                "Fragen oder Anmerkungen zu dieser Datenschutzbestimmung senden Sie bitte an <b>JN.AppDev@gmx.de</b><br><br>" +
                "Webversion: https://www.hs-aalen.de/pages/datenschutz-app</small></small>";

        String absturz_textinhalt = "<b>Absturzbenachrichtigungen</b><br>" +
                "<small><small>Sollten Sie im Falle eines Problems einen Absturzbericht über das Formular von <b>Google Play</b> absenden, werden geräte- (Hersteller, MAC, Android Version, etc.) und personenbezogene Daten (IP, etc.) unter Umständen erhoben, an das <b>Google Play Developer Center</b> übertragen und möglicherweise auch an Dritte übermittelt. Möchten Sie diesen Service nutzen, so machen Sie sich bitte mit den Datenschutzbestimmungen des Service von Google Inc. vertraut.<br>" +
                "<br>" +
                "Der Entwickler des <b>Aalener Optik-Formelrechners</b> und die Hochschule Aalen distanzieren sich an dieser Stelle ausdrücklich von diesem Service und übernehmen keine Haftung für Leistungen, die durch Google Inc. erfüllt werden. Gerne können Sie uns auch direkt eine Mail mit der Schilderung des Problems an die oben stehende Adresse senden, anstelle eines Absturzberichtes über das Formular von <b>Google Play</b>.</small></small>";

        String berechtigungen_textinhalt = "<b>Berechtigungen</b><br>" +
                "<small><small>Der <b>Aalener Optik-Formelrechner</b> benutzt keine Internetverbindung und empfängt oder verschickt dementsprechend auch keinerlei Daten. Es werden keine Dateien auf dem Gerät gespeichert und auch keine aus dem internen oder externen Speicher geladen. Er greift ebenfalls nicht auf den Standort des Gerätes, die SMS/MMS/Telefon-Funktion oder die Kontaktliste zu.<br>" +
                "<br>" +
                "Da das Interface sowohl im Portrait- als auch im Landscape-Modus verfügbar ist, verwendet er standardmäßig die Lagesensoren zur Ausrichtung des Bildschirminhalts, sofern diese Funktion in den Android Einstellungen nicht deaktiviert wurde.<br>" +
                "<br>" +
                "Darüber hinaus erfordert er keine Berechtigungen, auf das Gerät zuzugreifen.</small></small>";

        String datenverwendung_textinhalt = "<b>Auswertung von Nutzungsdaten</b><br>" +
                "<small><small>Der <b>Aalener Optik-Formelrechner</b> protokolliert und überträgt keine Nutzungsdaten.</small></small><br>" +
                "<br>" +
                "<b>Weitergabe von Daten an Dritte</b><br>" +
                "<small><small>Der <b>Aalener Optik-Formelrechner</b> übermittelt keine Daten an Dritte.</small></small><br>" +
                "<br>" +
                "<b>Standortdaten</b><br>" +
                "<small><small>Der <b>Aalener Optik-Formelrechner</b> erhebt keine Standortdaten.</small></small><br>" +
                "<br>" +
                "<b>Haftungshinweis</b><br>" +
                "<small><small>Trotz sorgfältiger Kontrolle aller Formeln haften weder der Entwickler noch die Hochschule Aalen für die Richtigkeit der berechneten Ergebnisse.</small></small>";

        String webseiten_textinhalt = "<b>Links zu Webseiten</b><br>" +
                "<small><small>Der <b>Aalener Optik-Formelrechner</b> verlinkt die Webseite der Hochschule Aalen, zu GitHub und den Lizenzbestimmungen von Apache als Klartext in der Informationsübersicht. Um auf die Webseite zu gelangen, müssen Sie die Seite manuell in Ihrem Browser aufrufen. Für die Datenschutzbestimmung der verlinkten Seiten ist der jeweilige Seitenbetreiber selbst verantwortlich.</small></small>";

        String lizenz_textinhalt = "<b>Lizenz</b><br>" +
                "<small><<b>Aalener Optik-Formelrechner</b></small><br>" +
                "<small><small>Copyright 2017-2018 René Nierath<br>" +
                "https://gitlab.com/HS_Aalen_RN/Aalener_Optik-Formelrechner</small></small><br>" +
                "<br>" +
                "<small><small>This program is free software: you can redistribute it and/or modify" +
                "it under the terms of the GNU General Public License as published by" +
                "the Free Software Foundation, either version 3 of the License, or" +
                "(at your option) any later version.<br>" +
                "<br>" +
                "This program is distributed in the hope that it will be useful," +
                "but WITHOUT ANY WARRANTY; without even the implied warranty of" +
                "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the" +
                "GNU General Public License for more details.<br>" +
                "<br>" +
                "You should have received a copy of the GNU General Public License" +
                "along with this program.  If not, see <br>https://www.gnu.org/licenses .<br>" +
                "<br>" +
                "Dieses Programm ist Freie Software: Sie können es unter den Bedingungen" +
                "der GNU General Public License, wie von der Free Software Foundation," +
                "Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren" +
                "veröffentlichten Version, weiter verteilen und/oder modifizieren.<br>" +
                "<br>" +
                "Dieses Programm wird in der Hoffnung bereitgestellt, dass es nützlich sein wird, jedoch" +
                "OHNE JEDE GEWÄHR,; sogar ohne die implizite" +
                "Gewähr der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK." +
                "Siehe die GNU General Public License für weitere Einzelheiten.<br>" +
                "<br>" +
                "Sie sollten eine Kopie der GNU General Public License zusammen mit diesem" +
                "Programm erhalten haben. Wenn nicht, siehe <br>https://www.gnu.org/licenses .</small></small>";

        String source_textinhalt = "<b>Externe Software</b><br>" +
                "<small><b>MPAndroidChart</b></small><br>" +
                "<small><small>Copyright 2018 Philipp Jahoda<br>" +
                "https://github.com/PhilJay/MPAndroidChart</small></small><br><br>" +

                "<small><small>Licensed under the Apache License, Version 2.0 (the \"License\"); you may not use this file except in compliance with the License. You may obtain a copy of the License at<br><br>" +
                "http://www.apache.org/licenses/LICENSE-2.0<br><br>" +
                "Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an \"AS IS\" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.</small></small>";

        String aenderungen_textinhalt = "<b>Änderung der Datenschutzerklärung</b><br>" +
                "<small><small>Bei Erweiterungen des Funktionsumfangs des <b>Aalener Optik-Formelrechners</b> oder durch neue rechtliche Vorgaben kann eine Änderung der Datenschutzerklärung folgen. In diesem Fall wird der Nutzer darüber informiert.</small></small><br>";

        datenschutz = (TextView)findViewById(R.id.datenschutz_text);
        datenschutz.setText(Html.fromHtml(datenschutz_textinhalt));

        absturz = (TextView) findViewById(R.id.absturz_text);
        absturz.setText(Html.fromHtml(absturz_textinhalt));

        berechtigungen = (TextView)findViewById(R.id.berechtigungen_text);
        berechtigungen.setText(Html.fromHtml(berechtigungen_textinhalt));

        datenverwendung = (TextView)findViewById(R.id.datenverwendung_text);
        datenverwendung.setText(Html.fromHtml(datenverwendung_textinhalt));

        webseiten = (TextView)findViewById(R.id.webseiten_text);
        webseiten.setText(Html.fromHtml(webseiten_textinhalt));

        lizenz = (TextView)findViewById(R.id.lizenz_text);
        lizenz.setText(Html.fromHtml(lizenz_textinhalt));

        source = (TextView)findViewById(R.id.source_text);
        source.setText(Html.fromHtml(source_textinhalt));

        aenderungen = (TextView)findViewById(R.id.aenderungen_text);
        aenderungen.setText(Html.fromHtml(aenderungen_textinhalt));

    }
}